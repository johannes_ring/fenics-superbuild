@echo off

set EXTRA_ARGS=%*

set NJOBS=1
set BUILD_TYPE=Release
set INSTALL_PREFIX=C:\fenics-snapshot\local
set SUPERBUILD_HOME=%~dp0\..

echo set PATH=%INSTALL_PREFIX%\bin;%INSTALL_PREFIX%\Scripts;%INSTALL_PREFIX%\lib;%%PATH%%> fenics.bat
echo set PKG_CONFIG_PATH=%INSTALL_PREFIX%\lib\pkgconfig;%%PKG_CONFIG_PATH%%>> fenics.bat
echo set PYTHONPATH=%INSTALL_PREFIX%\lib\site-packages;%%PYTHONPATH%%>> fenics.bat
echo set PYTHONPATH=%INSTALL_PREFIX%\lib\vtk-5.6;%%PYTHONPATH%%>> fenics.bat
echo set LDFLAGS=-Wl,--enable-auto-import>> fenics.bat

call fenics.bat

if not exist build-mingw\nul (
  mkdir build-mingw
)
cd build-mingw

cmake ^
    -G "MinGW Makefiles" ^
    -D CMAKE_BUILD_TYPE:STRING=%BUILD_TYPE% ^
    -D FEniCS_INSTALL_PREFIX:PATH=%INSTALL_PREFIX% ^
    -D FEniCS_BUILD_DEFAULT:BOOL=OFF ^
    -D FEniCS_BUILD_MinGW:BOOL=ON ^
    %SUPERBUILD_HOME%

mingw32-make -j %NJOBS%

cd ..

if not exist build-bootstrap\nul (
  mkdir build-bootstrap
)
cd build-bootstrap

cmake ^
    -G "MinGW Makefiles" ^
    -D CMAKE_BUILD_TYPE:STRING=%BUILD_TYPE% ^
    -D FEniCS_INSTALL_PREFIX:PATH=%INSTALL_PREFIX% ^
    -D FEniCS_BUILD_DEFAULT:BOOL=OFF ^
    -D FEniCS_BUILD_CMake:BOOL=ON ^
    -D FEniCS_BUILD_Bazaar:BOOL=OFF ^
    -D FEniCS_BUILD_GLib:BOOL=ON ^
    -D FEniCS_BUILD_pkgconfig:BOOL=ON ^
    -D FEniCS_BUILD_patch:BOOL=OFF ^
    -D FEniCS_BUILD_SWIG:BOOL=ON ^
    -D FEniCS_BUILD_setuptools:BOOL=ON ^
    %SUPERBUILD_HOME%

mingw32-make -j %NJOBS%

cd ..

if not exist build-fenics\nul (
  mkdir build-fenics
)
cd build-fenics

cmake ^
    -G "MinGW Makefiles" ^
    -D CMAKE_BUILD_TYPE:STRING=%BUILD_TYPE% ^
    -D FEniCS_INSTALL_PREFIX:PATH=%INSTALL_PREFIX% ^
    -D FEniCS_BUILD_DEFAULT:BOOL=ON ^
    -D FEniCS_BUILD_Boost:BOOL=ON ^
    -D FEniCS_BUILD_zlib:BOOL=ON ^
    -D FEniCS_BUILD_LibXml2:BOOL=ON ^
    -D FEniCS_BUILD_LAPACK:BOOL=ON ^
    -D FEniCS_BUILD_NumPy:BOOL=ON ^
    -D FEniCS_BUILD_ScientificPython:BOOL=ON ^
    -D FEniCS_BUILD_SuiteSparse:BOOL=ON ^
    -D FEniCS_BUILD_ParMETIS:BOOL=OFF ^
    -D FEniCS_BUILD_SPOOLES:BOOL=OFF ^
    -D FEniCS_BUILD_SCOTCH:BOOL=OFF ^
    -D FEniCS_BUILD_VTK:BOOL=ON ^
    -D FEniCS_BUILD_PETSc:BOOL=OFF ^
    -D FEniCS_BUILD_SLEPc:BOOL=OFF ^
    -D FEniCS_BUILD_CGAL:BOOL=ON ^
    -D CGAL_VERSION:STRING=3.6.1 ^
    -D FEniCS_BUILD_Armadillo:BOOL=ON ^
    -D FEniCS_BUILD_Trilinos:BOOL=OFF ^
    -D FEniCS_BUILD_IPython:BOOL=ON ^
    -D FEniCS_BUILD_PyReadline:BOOL=ON ^
    -D FEniCS_BUILD_PLY:BOOL=ON ^
    %EXTRA_ARGS% ^
    %SUPERBUILD_HOME%

mingw32-make -j %NJOBS%

cd ..
