SPOOLES is public domain, as described in lines 70-76 of
spooles.2.2.html:

This release is entirely within the public domain; 
there are no licensing restrictions,
and there is no warranty of any sort.
</b>
</p><p>
Contact <tt>cleve.ashcraft@boeing.com</tt> for more information, 
comments and bug reports.
