Copyright (C) 2003-2006 Gary Bishop.
Copyright (C) 2006  Jorgen Stenarson. <jorgen.stenarson@bostream.nu>

PyReadline is distributed under the terms of the BSD License.
