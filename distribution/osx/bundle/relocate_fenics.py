#!/usr/bin/env python

'''
Generate script to adjust FEnics.app rpath on OS X

usage:
   cd /Applications/FEnics.app
   python relocate_fenics.py > relocate_fenics.sh

   # check that everything looks ok in the generated script
   sh relocate_fenics.sh
'''

import os

libs = {}
exes = []
root = os.getcwd()

# find all Mach-64 executables
for name in os.popen('''find . \( -type f -o -type l \) -perm +111 | xargs file | grep 'Mach-O 64-bit' '''):
    name = name.strip().split(' ')[0].rstrip(':')
    libs[os.path.basename(name)] = name.lstrip('./')
    exes.append(name)

# generate the install_name_tool list
for exe in exes:
    deps = os.popen('''otool -L ''' + exe)
    l = 0
    for dep in deps:
        dep = dep.split()[0].strip()
        l += 1
        if l > 1 and not dep.startswith('/usr/') and not dep.startswith('/System/') and not dep.startswith(root): # skip line 1
            print 'install_name_tool -change %s %s %s' % (dep,os.path.join(root,libs[os.path.basename(dep)]),exe)
