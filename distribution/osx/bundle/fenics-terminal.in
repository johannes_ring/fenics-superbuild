#!/bin/bash

# Make sure XCode is installed
if ! [[ -x /usr/bin/xcodebuild ]]; then
    return=`/usr/bin/osascript <<EOF
tell app "System Events"
    Activate
    display dialog "This package requires XCode to be installed in order to run. Please install XCode from the OS X install disc and try again." buttons "OK" default button 1 with title "XCode Missing" with icon 0
end tell
EOF`
    exit 1
fi

# Make sure we run a compatible OS X version
OSX_CHECK_FAILED=false
case $(sw_vers -productVersion | cut -d '.' -f 1 -f 2) in
    10.6)
	if [ "@OSX_VERSION_SHORT@" != "10.6" ]; then
	    OSX_CHECK_FAILED=true
	fi
        ;;
    10.7)
	if [ "@OSX_VERSION_SHORT@" != "10.7" ]; then
	    OSX_CHECK_FAILED=true
	fi
        ;;
    10.8)
	if [ "@OSX_VERSION_SHORT@" != "10.8" ]; then
	    OSX_CHECK_FAILED=true
	fi
        ;;
    10.9)
	if [ "@OSX_VERSION_SHORT@" != "10.9" ]; then
	    OSX_CHECK_FAILED=true
	fi
        ;;
    10.10)
	if [ "@OSX_VERSION_SHORT@" != "10.10" ]; then
	    OSX_CHECK_FAILED=true
	fi
        ;;
    10.11)
	if [ "@OSX_VERSION_SHORT@" != "10.11" ]; then
	    OSX_CHECK_FAILED=true
	fi
        ;;
    *)
	OSX_CHECK_FAILED=true
        ;;
esac

if [ "${OSX_CHECK_FAILED}" = "true" ]; then
    return=`/usr/bin/osascript <<EOF
tell app "System Events"
    Activate
    display dialog "This package will not work with this version of Mac OS X." buttons "OK" default button 1 with title "OS X Version Mismatch Detected" with icon 0
end tell
EOF`
    exit 1
fi

# Make sure we use system Python
python_prefix=`python -c "import sys;print sys.prefix"`
if [ $python_prefix != "/System/Library/Frameworks/Python.framework/Versions/@PYTHON_VERSION_MAJOR@.@PYTHON_VERSION_MINOR@" ]; then
  return=`/usr/bin/osascript <<EOF
tell app "System Events"
    Activate
    display dialog "This package will only work with the Mac OS X system provided Python. Using Python installed manually from python.org or using Python from MacPorts, Fink, or similar will not work. Your copy of Python is located at $python_prefix." buttons "OK" default button 1 with title "Python Version Mismatch Detected" with icon 0
end tell
EOF`
  exit 1
fi

# Everything seems to be okay. Launch a terminal with PATH's
# set up to work with FEniCS.
/bin/bash --rcfile /Applications/FEniCS.app/Contents/Resources/share/fenics/fenics.conf -i
