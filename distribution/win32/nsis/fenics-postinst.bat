@echo off
set FENICS_DIR=%~dp0
set PATH=%FENICS_DIR%bin;%PATH%
cmake -DFENICS_DIR:PATH=%FENICS_DIR% -P path_substs.cmake
