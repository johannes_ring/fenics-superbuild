install(DIRECTORY "${ep_install_dir}/"
  DESTINATION .
  USE_SOURCE_PERMISSIONS
  COMPONENT RuntimeExecutables
  PATTERN "*"
  PATTERN "src" EXCLUDE
  PATTERN "msys" EXCLUDE
  PATTERN "bin/pexports.exe" EXCLUDE
  PATTERN "bin/mingw-get.exe" EXCLUDE
  PATTERN "libexec/mingw-get/*" EXCLUDE
  PATTERN "var/cache/mingw-get/*" EXCLUDE
  PATTERN "var/lib/mingw-get/*" EXCLUDE
  )

# Install software licenses
install(DIRECTORY "${PROJECT_SOURCE_DIR}/distribution/licenses"
  DESTINATION .
  USE_SOURCE_PERMISSIONS
  )

if (MINGW)
  if (NOT "${MINGW_DIR}" STREQUAL "${ep_install_dir}")
    install(DIRECTORY "${MINGW_DIR}/"
      DESTINATION .
      USE_SOURCE_PERMISSIONS
      COMPONENT RuntimeExecutables
      PATTERN "*"
      PATTERN "msys" EXCLUDE
      #PATTERN "mingw32" EXCLUDE
      )
  endif()
endif()

if (NOT "${PYTHON_DIR}" STREQUAL "${ep_install_dir}")
  install(DIRECTORY "${PYTHON_DIR}"
    DESTINATION lib
    USE_SOURCE_PERMISSIONS
    COMPONENT RuntimeExecutables
    )
endif()

add_subdirectory(nsis)
