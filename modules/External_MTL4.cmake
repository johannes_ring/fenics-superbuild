#
# MTL4
#

set(proj MTL4)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if (${proj_UPPER}_EXECUTABLE AND NOT EXISTS ${${proj_UPPER}_EXECUTABLE})
  message(FATAL_ERROR "${proj_UPPER}_EXECUTABLE variable is defined but it corresponds to a non-existing executable.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_EXECUTABLE)
  set(proj_URL "http://osl.iu.edu/download/research/mtl/mtl4-beta-1-r6916.tar.gz")
  set(proj_URL_MD5 "76456a777b52d20da25e7ed7152807fb")

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_directory ${ep_source_dir}/${proj}/boost ${ep_install_dir}/include/boost
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
