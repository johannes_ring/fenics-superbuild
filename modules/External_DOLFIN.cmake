#
# DOLFIN
#

set(proj DOLFIN)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  if (FEniCS_BUILD_STABLE)
    if ("${DOLFIN_VERSION}" VERSION_LESS "1.1")
      set(proj_URL "http://launchpad.net/dolfin/1.0.x/${proj_VERSION}/+download/dolfin-${proj_VERSION}.tar.gz")
    elseif ("${DOLFIN_VERSION}" VERSION_LESS "1.2")
      set(proj_URL "http://launchpad.net/dolfin/1.1.x/${proj_VERSION}/+download/dolfin-${proj_VERSION}.tar.gz")
    elseif ("${DOLFIN_VERSION}" VERSION_LESS "1.2")
      set(proj_URL "http://launchpad.net/dolfin/1.2.x/${proj_VERSION}/+download/dolfin-${proj_VERSION}.tar.gz")
    else()
      set(proj_URL "https://bitbucket.org/fenics-project/dolfin/downloads/dolfin-${proj_VERSION}.tar.gz")
    endif()
    set(proj_URL_MD5_0.9.9 "57a892f9668abd5ee79c645475422c6a")
    set(proj_URL_MD5_0.9.10 "7224edfa449737821e37f0411709df30")
    set(proj_URL_MD5_0.9.11 "62ae018537fef6b7329de5e7bb2f3588")
    set(proj_URL_MD5_1.0-beta "bded9c6eaea8ff7832ac9f76ff946eda")
    set(proj_URL_MD5_1.0-beta2 "bfda7c13dab57c1badfb35e8f0942663")
    set(proj_URL_MD5_1.0-rc1 "4da3699a5481c944bb87675c06c82667")
    set(proj_URL_MD5_1.0-rc2 "da0b04ad9ecac170f2c48186b7f37792")
    set(proj_URL_MD5_1.0.0 "f2d27de414c2e1768a03464bb38fef38")
    set(proj_URL_MD5_1.0.1 "579237daa095b7c333670e2ffa7b8e0d")
    set(proj_URL_MD5_1.1.0 "73611c94246f99199d927cf6b647e289")
    set(proj_URL_MD5_1.2.0 "2efc215febc77f25fe24f0adb627e4cf")
    set(proj_URL_MD5_1.3.0 "bc5b9f18ee7738b2e9e6768c7aa57c58")
    set(proj_URL_MD5_1.4.0 "b736119d524590b6bd0484f09893e018")
    set(proj_URL_MD5_1.5.0 "071a4a2ca3c2fc00657bd09457ebed54")
    set(proj_URL_MD5_1.6.0 "35cb4baf7ab4152a40fb7310b34d5800")
    set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")
  else()
    set(proj_URL "http://fenicsproject.org/pub/software/dolfin/dolfin-snapshot.tar.gz")
    set(proj_URL_MD5 "")
  endif()

  set(proj_PATCH_COMMAND "")
  if ("${proj_VERSION}" STREQUAL "1.0-beta")
    set(proj_PATCH_COMMAND
      ${PATCH_EXECUTABLE} -p1 --forward --batch < ${CMAKE_CURRENT_SOURCE_DIR}/patches/dolfin-1.0-beta.patch)
  elseif ("${proj_VERSION}" VERSION_EQUAL "0.9.9")
    set(proj_PATCH_COMMAND
      ${PATCH_EXECUTABLE} -p1 --forward --batch < ${CMAKE_CURRENT_SOURCE_DIR}/patches/dolfin-0.9.9.patch)
  elseif (("${proj_VERSION}" VERSION_EQUAL "1.3.0") OR ("${proj_VERSION}" VERSION_EQUAL "1.4.0"))
    set(proj_PATCH_COMMAND
      ${PATCH_EXECUTABLE} -p1 --forward --batch < ${CMAKE_CURRENT_SOURCE_DIR}/patches/dolfin-petsc-no-x11.patch)
  elseif ("${proj_VERSION}" VERSION_EQUAL "1.5.0")
    set(proj_PATCH_COMMAND
      ${PATCH_EXECUTABLE} -p1 --forward --batch < ${CMAKE_CURRENT_SOURCE_DIR}/patches/dolfin-suitesparse_long.patch)
  elseif (("${proj_VERSION}" STREQUAL "snapshot") OR ("${proj_VERSION}" VERSION_EQUAL "1.6.0"))
    set(proj_PATCH_COMMAND
      ${PATCH_EXECUTABLE} -p1 --forward --batch < ${CMAKE_CURRENT_SOURCE_DIR}/patches/dolfin-1.6.patch)
  endif()

  set(CMAKE_ARGS
    ${ep_common_args}
    #-D CMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
    -D CMAKE_VERBOSE_MAKEFILE:BOOL=OFF
    -D DOLFIN_ENABLE_UNIT_TESTS:BOOL=OFF
    -D DOLFIN_ENABLE_TESTING:BOOL=OFF
    -D CMAKE_SKIP_RPATH:BOOL=ON
    -D CMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=OFF
    -D SWIG_EXECUTABLE:FILEPATH=${SWIG_EXECUTABLE}
    )

  if (DEFINED ARMADILLO_DIR)
    list(APPEND CMAKE_ARGS -D ARMADILLO_DIR:PATH=${ARMADILLO_DIR})
  endif()

  if (DEFINED CGAL_DIR)
    list(APPEND CMAKE_ARGS -D CGAL_DIR:PATH=${CGAL_DIR})
  endif()

  if (DEFINED ATLAS_DIR)
    # FIXME: Not tested on Windows
    list(APPEND CMAKE_ARGS
      -D BLA_VENDOR:STRING=ATLAS
      -D LAPACK_DIR:PATH=${ATLAS_DIR}
      -D BLAS_DIR:PATH=${ATLAS_DIR}
      -D LAPACK_LIBRARIES:FILEPATH=${ATLAS_DIR}/lib/liblapack.a
      -D BLAS_LIBRARIES:FILEPATH=${ATLAS_DIR}/lib/libatlas.a
      )
  elseif (DEFINED LAPACK_DIR)
    list(APPEND CMAKE_ARGS
      -D LAPACK_DIR:PATH=${LAPACK_DIR}
      -D BLAS_DIR:PATH=${LAPACK_DIR}
      )
  endif()

  if (DEFINED SUITESPARSE_DIR)
    list(APPEND CMAKE_ARGS
      -D AMD_DIR:PATH=${SUITESPARSE_DIR}
      -D CAMD_DIR:PATH=${SUITESPARSE_DIR}
      -D COLAMD_DIR:PATH=${SUITESPARSE_DIR}
      -D CCOLAMD_DIR:PATH=${SUITESPARSE_DIR}
      -D CHOLMOD_DIR:PATH=${SUITESPARSE_DIR}
      -D UMFPACK_DIR:PATH=${SUITESPARSE_DIR}
      )
  endif()

  if (DEFINED PETSC_DIR)
    list(APPEND CMAKE_ARGS -D PETSC_DIR:PATH=${PETSC_DIR})
  endif()

  if (DEFINED SLEPC_DIR)
    list(APPEND CMAKE_ARGS -D SLEPC_DIR:PATH=${SLEPC_DIR})
  endif()

  if (DEFINED MTL4_DIR)
    list(APPEND CMAKE_ARGS -D MTL4_DIR:PATH=${MTL4_DIR})
  endif()

  if (DEFINED ZLIB_DIR)
    list(APPEND CMAKE_ARGS
      -D ZLIB_INCLUDE_DIRS:PATH=${ZLIB_INCLUDE_DIRS}
      -D ZLIB_LIBRARY:FILEPATH=${ZLIB_LIBRARY}
      )
  endif()

  if (DEFINED HDF5_DIR)
    list(APPEND CMAKE_ARGS -D HDF5_DIR=${HDF5_DIR})
  endif()

  if (APPLE)
    list(APPEND CMAKE_ARGS -D DOLFIN_ENABLE_QT:BOOL=OFF)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    PATCH_COMMAND ${proj_PATCH_COMMAND}
    CMAKE_GENERATOR ${CMAKE_GENERATOR}
    CMAKE_ARGS ${CMAKE_ARGS}
    )

  if (FEniCS_FORCE_REBUILD)
    externalproject_add_step(${proj} force_rebuild
      COMMENT "Force ${proj} re-build"
      DEPENDERS download
      ALWAYS 1
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )
  endif()

  if (FEniCS_VERSION STREQUAL "snapshot")
    externalproject_add_step(${proj} generate_form_files
      COMMAND "cmake/scripts/generate-form-files"
      COMMENT "${proj} generate form files"
      DEPENDEES download
      DEPENDERS configure
      WORKING_DIRECTORY ${ep_source_dir}/${proj}
      )
    externalproject_add_step(${proj} generate_cmakefiles
      COMMAND "cmake/scripts/generate-cmakefiles"
      COMMENT "${proj} generate CMake files"
      DEPENDEES download
      DEPENDERS configure
      WORKING_DIRECTORY ${ep_source_dir}/${proj}
      )
    externalproject_add_step(${proj} generate_swig_docstrings
      COMMAND "cmake/scripts/generate-swig-docstrings"
      COMMENT "${proj} generate SWIG docstrings"
      DEPENDEES download
      DEPENDERS configure
      WORKING_DIRECTORY ${ep_source_dir}/${proj}
      )
    externalproject_add_step(${proj} generate_swig_interface
      COMMAND "cmake/scripts/generate-swig-interface"
      COMMENT "${proj} generate SWIG interface"
      DEPENDEES download
      DEPENDERS configure
      WORKING_DIRECTORY ${ep_source_dir}/${proj}
      )
    externalproject_add_step(${proj} download_demo_data
      COMMAND "cmake/scripts/download-demo-data"
      COMMENT "${proj} download demo data"
      DEPENDEES download
      DEPENDERS configure
      WORKING_DIRECTORY ${ep_source_dir}/${proj}
      )
  endif()

else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
