#
# NumPy
#

set(proj NumPy)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  set(proj_URL "http://downloads.sourceforge.net/project/numpy/NumPy/${proj_VERSION}/numpy-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_1.5.0 "3a8bfdc434df782d647161c48943ee09")
  set(proj_URL_MD5_1.6.1 "2bce18c08fc4fce461656f0f4dd9103e")
  set(proj_URL_MD5_1.6.2 "95ed6c9dcc94af1fc1642ea2a33c1bba")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  set(proj_additional_configure_args)
  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_configure_args "--compiler=mingw32")
    set(proj_additional_build_args "--compiler=mingw32")
    set(proj_additional_install_args "--no-compile")
  endif()

  # To configure NumPy we run a cmake -P script.
  # The script will create a site.cfg file and
  # then run python setup.py config to verify setup.
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_configure_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
