#
# pkgconfig
#

set(proj pkgconfig)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})

if (FEniCS_BUILD_${proj})
  if (WIN32)
    # We use prebuilt binaries on Windows (requires GLib dll's).
    set(proj_URL "http://ftp.gnome.org/pub/gnome/binaries/win32/dependencies/pkg-config_0.23-3_win32.zip")
    set(proj_URL_MD5 "2af054f5abacb6dd6c272d6b6714da18")
  else()
    #set(proj_URL "http://pkgconfig.freedesktop.org/releases/pkg-config-0.25.tar.gz")
    #set(proj_URL_MD5 "a3270bab3f4b69b7dc6dbdacbcae9745")
    set(proj_URL "http://pkgconfig.freedesktop.org/releases/pkg-config-0.28.tar.gz")
    set(proj_URL_MD5 "aa3c86e67551adc3ac865160e34a2a0d")
  endif()

  if (MINGW)
    # FIXME: Can we use standard DOWNLOAD_COMMAND on Windows also?
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_download_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_step-mingw.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_install_step-mingw.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step-mingw.cmake @ONLY)

    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      DOWNLOAD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_step-mingw.cmake
      CONFIGURE_COMMAND ""
      BUILD_COMMAND ""
      INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step-mingw.cmake
      )
  else()
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ${ep_source_dir}/${proj}/configure --prefix=${ep_install_dir} --with-pc-path=/usr/lib/pkgconfig --with-internal-glib
      )
  endif()
endif()
