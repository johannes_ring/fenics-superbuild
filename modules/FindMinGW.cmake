# - this module looks for MinGW
#

if (WIN32)
  find_path(MINGW_INSTALL_PATH
    NAMES bin/mingw32-make
    HINTS ${MINGW_DIR} $ENV{MINGW_DIR}
    PATHS "C:/MinGW"
    )

  mark_as_advanced(MINGW_INSTALL_PATH)
endif()
