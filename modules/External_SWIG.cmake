#
# SWIG
#

set(proj SWIG)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if (${proj_UPPER}_EXECUTABLE AND NOT EXISTS ${${proj_UPPER}_EXECUTABLE})
  message(FATAL_ERROR "${proj_UPPER}_EXECUTABLE variable is defined but it corresponds to a non-existing executable.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_EXECUTABLE)
  if (MSVC)
    set(SWIG_VERSION "2.0.1")
    set(proj_URL "http://downloads.sourceforge.net/swig/swigwin-${SWIG_VERSION}.zip")
    set(proj_URL_MD5_2.0.1 "750bf2c129c6670ed2753a0fa6a80d12")
  else()
    set(proj_URL "http://downloads.sourceforge.net/swig/swig-${SWIG_VERSION}.tar.gz")
    set(proj_URL_MD5_2.0.0 "36ee2d9974be46a9f0a36460af928eb9")
    set(proj_URL_MD5_2.0.3 "e548ea3882b994c4907d6be86bef90f2")
    set(proj_URL_MD5_2.0.4 "4319c503ee3a13d2a53be9d828c3adc0")
    set(proj_URL_MD5_2.0.7 "8c2f6a9f51677647a64c8adb1b176299")
    set(proj_URL_MD5_2.0.8 "69f917e870efc0712c06ab53217b28d1")
    set(proj_URL_MD5_2.0.9 "54d534b14a70badc226129159412ea85")
    set(proj_URL_MD5_3.0.5 "dcb9638324461b9baba8e044fe59031d")
  endif()
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  set(proj_PATCH_COMMAND "")
  if (MINGW)
    set(proj_PATCH_COMMAND
      ${PATCH_EXECUTABLE} -p1 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/swig_windows.patch)
  endif()

  if (MSVC)
    # We run cmake -P scripts to install this external project.
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_install_step-win32.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step-win32.cmake @ONLY)
  
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ""
      BUILD_COMMAND ""
      INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step-win32.cmake
      )
  else()
    set(ep_additional_configure_args)
    if ("${SWIG_VERSION}" VERSION_EQUAL "2.0.4" OR "${SWIG_VERSION}" VERSION_GREATER "2.0.4")
      if (DEFINED PCRE_DIR)
	set(ep_additional_configure_args --with-pcre-prefix=${PCRE_DIR})
      endif()
    endif()

    # We run cmake -P scripts to configure, build, and
    # install this external project.
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_configure_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_build_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_install_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake @ONLY)
  
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      PATCH_COMMAND ${proj_PATCH_COMMAND}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
      BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake
      INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake
      )
  endif()
  
  set(SWIG_DIR ${ep_install_dir})
  if (WIN32)
    set(SWIG_EXECUTABLE ${SWIG_DIR}/bin/swig.exe)
    set(SWIG_LIB ${SWIG_DIR}/share/swig/2.0.1)
  else()
    set(SWIG_EXECUTABLE ${SWIG_DIR}/bin/swig)
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")

  if (NOT DEFINED SWIG_EXECUTABLE)
    find_package(SWIG)
  endif()
endif()

# Add SWIG_EXECUTABLE to common args
set(ep_common_args
  ${ep_common_args}
  -D SWIG_EXECUTABLE:FILEPATH=${SWIG_EXECUTABLE}
  )
