#
# Python
#

set(proj Python)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if (DEFINED ${proj_UPPER}_EXECUTABLE AND NOT EXISTS ${${proj_UPPER}_EXECUTABLE})
  message(FATAL_ERROR "${proj_UPPER}_EXECUTABLE variable is defined but it corresponds to a non-existing executable.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_EXECUTABLE)
  set(proj_URL "http://python.org/ftp/python/${proj_VERSION}/Python-${proj_VERSION}.tgz")
  set(proj_URL_MD5_2.6.6 "b2f209df270a33315e62c1ffac1937f0")
  set(proj_URL_MD5_2.7.2 "0ddfe265f1b3d0a8c2459f5bf66894c7")
  set(proj_URL_MD5_2.7.3 "2cf641732ac23b18d139be077bd906cd")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  string(SUBSTRING ${proj_VERSION} 0 1 PYTHON_VERSION_MAJOR)
  string(SUBSTRING ${proj_VERSION} 2 1 PYTHON_VERSION_MINOR)
  string(SUBSTRING ${proj_VERSION} 4 1 PYTHON_VERSION_PATCH)

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${ep_source_dir}/${proj}/configure --prefix=${ep_install_dir}
    )

  set(PYTHON_DIR ${ep_install_dir})

  # Set Python executable
  if (WIN32)
    set(PYTHON_INCLUDE_DIR "${PYTHON_DIR}/include")
    set(PYTHON_EXECUTABLE "${PYTHON_DIR}/python.exe")
  else()
    set(PYTHON_INCLUDE_DIR "${PYTHON_DIR}/include/python${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}")
    set(PYTHON_EXECUTABLE "${PYTHON_DIR}/bin/python")
  endif()

  # Set Python library
  if (MINGW)
    set(PYTHON_LIBRARY "${PYTHON_DIR}/libs/libpython${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}.a")
  elseif (MSVC)
    set(PYTHON_LIBRARY "${PYTHON_DIR}/libs/python${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}.lib")
  elseif (APPLE)
    set(PYTHON_LIBRARY "${PYTHON_DIR}/lib/libpython${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}.dylib")
  else()
    set(PYTHON_LIBRARY "${PYTHON_DIR}/lib/libpython${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}.so")
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")

  if (NOT DEFINED PYTHON_EXECUTABLE)
    find_package(PythonInterp)
  endif()

  # Find Python version numbers
  execute_process(
    COMMAND ${PYTHON_EXECUTABLE} -c "import sys;print sys.version_info[0]"
    OUTPUT_VARIABLE PYTHON_VERSION_MAJOR
    OUTPUT_STRIP_TRAILING_WHITESPACE)
  execute_process(
    COMMAND ${PYTHON_EXECUTABLE} -c "import sys;print sys.version_info[1]"
    OUTPUT_VARIABLE PYTHON_VERSION_MINOR
    OUTPUT_STRIP_TRAILING_WHITESPACE)
  execute_process(
    COMMAND ${PYTHON_EXECUTABLE} -c "import sys;print sys.version_info[2]"
    OUTPUT_VARIABLE PYTHON_VERSION_PATCH
    OUTPUT_STRIP_TRAILING_WHITESPACE)

  if (NOT DEFINED PYTHON_DIR)
    if (WIN32)
      string(REPLACE "/python.exe" "" PYTHON_DIR "${PYTHON_EXECUTABLE}")
    else()
      string(REGEX REPLACE "/bin/python.*" "" PYTHON_DIR "${PYTHON_EXECUTABLE}")
    endif()
  endif()

  if (NOT DEFINED PYTHON_INCLUDE_DIR)
    if (WIN32)
      set(PYTHON_INCLUDE_DIR "${PYTHON_DIR}/include")
    else()
      set(PYTHON_INCLUDE_DIR "${PYTHON_DIR}/include/python${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}")
    endif()
  endif()

  if (NOT DEFINED PYTHON_LIBRARY)
    if (MINGW)
      set(PYTHON_LIBRARY "${PYTHON_DIR}/libs/libpython${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}.a")
    elseif (MSVC)
      set(PYTHON_LIBRARY "${PYTHON_DIR}/libs/python${PYTHON_VERSION_MAJOR}${PYTHON_VERSION_MINOR}.lib")
    elseif (APPLE)
      set(PYTHON_LIBRARY "${PYTHON_DIR}/lib/libpython${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}.dylib")
    else()
      set(PYTHON_LIBRARY "${PYTHON_DIR}/lib/libpython${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}.so")
    endif()
  endif()
endif()

# Make sure site-packages directory exists
if (WIN32)
  file(MAKE_DIRECTORY "${ep_install_dir}/lib/site-packages")
else()
  file(MAKE_DIRECTORY "${ep_install_dir}/lib/python${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}/site-packages")
endif()

# Add PYTHON_EXECUTABLE, PYTHON_INCLUDE_DIR, and PYTHON_LIBRARY to common args
set(ep_common_args
  ${ep_common_args}
  -D PYTHON_EXECUTABLE:FILEPATH=${PYTHON_EXECUTABLE}
  -D PYTHON_INCLUDE_DIR:PATH=${PYTHON_INCLUDE_DIR}
  -D PYTHON_LIBRARY:FILEPATH=${PYTHON_LIBRARY}
  )
