#
# matplotlib
#

set(proj matplotlib)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})

if (FEniCS_BUILD_${proj})
  set(proj_URL "http://downloads.sourceforge.net/project/matplotlib/matplotlib/matplotlib-1.0/matplotlib-1.0.0.tar.gz")
  set(proj_URL_MD5 "7d964de548c9887917762a11e0ffecd3")

  set(proj_additional_configure_args)
  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_configure_args "--compiler=mingw32")
    set(proj_additional_build_args "--compiler=mingw32")
    set(proj_additional_install_args "--no-compile")
  endif()

  # To configure matplotlib we run a cmake -P script.
  # The script will create a site.cfg file and
  # then run python setup.py config to verify setup.
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_configure_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
