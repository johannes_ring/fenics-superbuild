#
# Bazaar
#

set(proj Bazaar)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})

# Sanity checks
if (DEFINED BZR_EXECUTABLE AND NOT EXISTS "${BZR_EXECUTABLE}")
  message(FATAL_ERROR "BZR_EXECUTABLE variable is defined but it corresponds to a non-existing executable.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED BZR_EXECUTABLE)
  set(proj_URL "http://launchpad.net/bzr/2.2/2.2.1/+download/bzr-2.2.1.tar.gz")
  set(proj_URL_MD5 "8a6abd8f888fb3e0845e2044d41a24bc")

  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_build_args "--compiler=mingw32")
    set(proj_additional_install_args "--no-compile")
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
  
  if (NOT DEFINED BZR_EXECUTABLE)
    find_program(BZR_EXECUTABLE bzr)
  endif()
endif()
