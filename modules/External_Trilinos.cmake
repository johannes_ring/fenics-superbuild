#
# Trilinos
#

set(proj Trilinos)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  set(proj_URL "http://trilinos.sandia.gov/download/files/trilinos-${proj_VERSION}-Source.tar.gz")
  set(proj_URL_MD5_10.6.2 "686d44017c0d944927979ea19eac5624")
  set(proj_URL_MD5_10.8.2 "31532fdfb52d44cce751f67f91290880")
  set(proj_URL_MD5_10.10.2 "0bb1116a438ac3b7a070ded153ee229f")
  set(proj_URL_MD5_10.12.2 "eafdfb5d702b7ae1ce6146db2233bc94")
  set(proj_URL_MD5_11.0.3 "41368eb4c589e07a7f8e7de635ca234a")
  set(proj_URL_MD5_11.4.3 "50681bb44341300154594c52a4dafe80")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  set(proj_PATCH_COMMAND)
  if ("${proj_VERSION}" VERSION_EQUAL "11.4.3")
    set(proj_PATCH_COMMAND
      ${PATCH_EXECUTABLE} -p1 --forward --batch --binary < ${CMAKE_SOURCE_DIR}/patches/trilinos-11.4.3.patch)
  endif()

  set(CMAKE_ARGS
    ${ep_common_args}
    -D CMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
    -D CMAKE_VERBOSE_MAKEFILE:BOOL=OFF
    -D Trilinos_ENABLE_ALL_OPTIONAL_PACKAGES:BOOL=ON
    -D Trilinos_ENABLE_Epetra:BOOL=ON
    -D Trilinos_ENABLE_ML:BOOL=ON
    -D Trilinos_ENABLE_PyTrilinos:BOOL=ON
    -D Trilinos_ENABLE_TESTS:BOOL=OFF
    -D Trilinos_ENABLE_EXAMPLES:BOOL=OFF
    -D Trilinos_INSTALL_INCLUDE_DIR:PATH=include/trilinos
    -D PyTrilinos_INSTALL_PREFIX:PATH=${ep_install_dir}
    )

  if (EXISTS SWIG_EXECUTABLE)
    list(APPEND CMAKE_ARGS -D SWIG_EXECUTABLE:FILEPATH=${SWIG_EXECUTABLE})
  endif()

  if (DEFINED ATLAS_DIR)
    list(APPEND CMAKE_ARGS
      -D BLAS_LIBRARY_NAMES:STRING=\"cblas;f77blas;atlas\"
      -D BLAS_LIBRARY_DIRS:PATH=${ATLAS_DIR}
      -D LAPACK_LIBRARY_DIRS:PATH=${ATLAS_DIR}
      )
  elseif (DEFINED LAPACK_DIR)
    list(APPEND CMAKE_ARGS
      -D BLAS_LIBRARY_DIRS:PATH=${LAPACK_DIR}
      -D LAPACK_LIBRARY_DIRS:PATH=${LAPACK_DIR}
      )
  endif()

  if (WIN32)
    list(APPEND CMAKE_ARGS -D TPL_ENABLE_MPI:BOOL=OFF)
  else()
    list(APPEND CMAKE_ARGS -D TPL_ENABLE_MPI:BOOL=ON)
  endif()

  if (NOT CMAKE_Fortran_COMPILER_WORKS)
    list(APPEND CMAKE_ARGS -D Trilinos_ENABLE_Fortran:BOOL=OFF)
  else()
    list(APPEND CMAKE_ARGS -D Trilinos_ENABLE_Fortran:BOOL=ON)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    PATCH_COMMAND ${proj_PATCH_COMMAND}
    CMAKE_GENERATOR ${CMAKE_GENERATOR}
    CMAKE_ARGS ${CMAKE_ARGS}
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
    fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
