#
# PyReadline
#

set(proj PyReadline)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})

if (FEniCS_BUILD_${proj})
  set(proj_URL "http://launchpad.net/pyreadline/1.6/1.6.1/+download/pyreadline-1.6.1.zip")
  set(proj_URL_MD5 "e5b4189bced314ff858e8b175df86a01")

  set(proj_additional_configure_args)
  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_configure_args "--compiler=mingw32")
    set(proj_additional_build_args "--compiler=mingw32")
    set(proj_additional_install_args "--no-compile")
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
