#
# MinGW
#

set(proj MinGW)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})

if (FEniCS_BUILD_${proj})
  set(proj_URL "http://downloads.sourceforge.net/project/mingw/Installer/mingw-get/mingw-get-0.5-beta-20120426-1/mingw-get-0.5-mingw32-beta-20120426-1-bin.zip")
  set(proj_URL_MD5 "52848b60260a1292412ad6e9f92b46b7")
  #set(proj_URL "http://downloads.sourceforge.net/project/mingw/Automated%20MinGW%20Installer/mingw-get/mingw-get-0.3-alpha-2.1/mingw-get-0.3-mingw32-alpha-2.1-bin.zip")
  #set(proj_URL_MD5 "d69a012dd376b361876df62d972f5f25")

  # We run cmake -P scripts to configure and install this external project.
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_configure_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_install_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake @ONLY)

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
    BUILD_COMMAND ""
    INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake
    )
endif()
