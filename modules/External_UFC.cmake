#
# UFC
#

set(proj UFC)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  if (FEniCS_BUILD_STABLE)
    if ("${UFC_VERSION}" VERSION_LESS "2.1.0")
      # Handle bug in URL for UFC version 2.0.0
      if ("${UFC_VERSION}" VERSION_EQUAL "2.0.0")
	set(proj_URL "http://launchpad.net/ufc/2.0.x/2.0/+download/ufc-${proj_VERSION}.tar.gz")
      else()
	set(proj_URL "http://launchpad.net/ufc/2.0.x/${proj_VERSION}/+download/ufc-${proj_VERSION}.tar.gz")
      endif()
    elseif ("${UFC_VERSION}" VERSION_LESS "2.2.0")
      set(proj_URL "http://launchpad.net/ufc/2.1.x/${proj_VERSION}/+download/ufc-${proj_VERSION}.tar.gz")
    elseif ("${UFC_VERSION}" VERSION_LESS "2.3.0")
      set(proj_URL "http://launchpad.net/ufc/2.2.x/${proj_VERSION}/+download/ufc-${proj_VERSION}.tar.gz")
    else()
      set(proj_URL "https://bitbucket.org/fenics-project/ufc-deprecated/downloads/ufc-${proj_VERSION}.tar.gz")
    endif()
    set(proj_URL_MD5_1.4.2 "a1e6f1a7914e1cbd644fd00c28397d98")
    set(proj_URL_MD5_2.0.0 "9d7c5a355b1233b8662d12e3caff1432")
    set(proj_URL_MD5_2.0.1 "3f39c2ae490d2ce3a6420c622cab3b73")
    set(proj_URL_MD5_2.0.2 "401b8fc24be5bc77adbbfecb332c4ef9")
    set(proj_URL_MD5_2.0.3 "54c2e1a8e2949de0232cbfde9dba39fd")
    set(proj_URL_MD5_2.0.4 "e6b47cb48eb8340900449caf9bde1700")
    set(proj_URL_MD5_2.0.5 "7edcdd12cbd2d4a0525aa017281771c4")
    set(proj_URL_MD5_2.0.6 "12578397e4d4e5f9da5a246d74330c1a")
    set(proj_URL_MD5_2.1.0 "73354381d11a925c0848ea61194fdbde")
    set(proj_URL_MD5_2.2.0 "1066de17b67e65d93b0d58a18661a0fc")
    set(proj_URL_MD5_2.3.0 "11ee6ff8b7b44705932c6ebc6ae265d3")
    set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")
  else()
    set(proj_URL "http://fenicsproject.org/pub/software/ufc/ufc-snapshot.tar.gz")
    set(proj_URL_MD5 "")
  endif()

  if ("${proj_VERSION}" STREQUAL "snapshot")
    set(PATCH_COMMAND "")
  elseif ("${proj_VERSION}" VERSION_LESS "2.0.2")
    set(PATCH_COMMAND
      ${PATCH_EXECUTABLE} -p0 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/ufc.patch)
  else()
    set(PATCH_COMMAND "")
  endif()

  set(ep_additional_args)
  if (WIN32)
    # FIXME: Is this necessary?
    set(ep_additional_args
      -D UFC_INSTALL_PYTHON_MODULE_DIR:PATH=lib/site-packages
      -D UFC_INSTALL_PYTHON_EXT_DIR:PATH=lib/site-packages)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    PATCH_COMMAND ${PATCH_COMMAND}
    DOWNLOAD_DIR ${ep_download_dir}
    CMAKE_GENERATOR ${CMAKE_GENERATOR}
    CMAKE_ARGS
      ${ep_common_args}
      ${ep_additional_args}
      -D CMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
      -D CMAKE_VERBOSE_MAKEFILE:BOOL=OFF
      -D SWIG_EXECUTABLE:FILEPATH=${SWIG_EXECUTABLE}
      ${Boost_CMAKE_ARGS}
    )

  if (FEniCS_FORCE_REBUILD)
    externalproject_add_step(${proj} force_rebuild
      COMMENT "Force ${proj} re-build"
      DEPENDERS download
      ALWAYS 1
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
