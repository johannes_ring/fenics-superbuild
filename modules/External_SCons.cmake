#
# SCons
#

set(proj SCons)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})

if (FEniCS_BUILD_${proj})
  set(proj_URL "http://prdownloads.sourceforge.net/scons/scons-1.3.0.tar.gz")
  set(proj_URL_MD5 "ad6838c867abd2ad5bf371b353d594f7")

  set(proj_additional_configure_args)
  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_install_args ${proj_additional_install_args} --no-compile)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
