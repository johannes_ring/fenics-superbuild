
#------------------------------------------------------------------------------
# Convenient macro allowing to define an "empty" project in case an external
# one is provided using for example <proj>_DIR. Doing so allows to keep
# the external project dependency system happy.
#------------------------------------------------------------------------------

macro(FEniCSMacro_Empty_ExternalProject proj dependencies)
  externalproject_add(${proj}
    DEPENDS ${dependencies}
    DOWNLOAD_COMMAND ""
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
    )
endmacro()
