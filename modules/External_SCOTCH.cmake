#
# SCOTCH
#

set(proj SCOTCH)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  if ("${proj_VERSION}" VERSION_EQUAL "6.0.0")
    set(proj_URL "https://gforge.inria.fr/frs/download.php/31832/scotch_6.0.0_esmumps.tar.gz")
    set(proj_URL_MD5 "c50d6187462ba801f9a82133ee666e8e")
    set(proj_EXTRACTSTO "scotch_6.0.0_esmumps")
  elseif ("${proj_VERSION}" VERSION_EQUAL "5.1.10a")
    set(proj_URL "https://gforge.inria.fr/frs/download.php/27436/scotch_5.1.10a.tar.gz")
    set(proj_URL_MD5 "0f9820cba174d171ceb2f30ad8649261")
    set(proj_EXTRACTSTO "scotch_5.1.10a")
  else()
    set(proj_URL "https://gforge.inria.fr/frs/download.php/28977/scotch_5.1.12b.tar.gz")
    set(proj_URL_MD5 "5d912599c2521b1ecbcd8d12b68eef9c")
    set(proj_EXTRACTSTO "scotch_5.1.12")
  endif()

  # Set SCOTCH_ARCH if not already defined
  if (NOT DEFINED SCOTCH_ARCH)
    if (APPLE)
      set(SCOTCH_ARCH "i686_mac_darwin8")
    elseif (MINGW)
      # FIXME: This is not tested!
      set(SCOTCH_ARCH "i686_pc_mingw32")
    else()
      set(SCOTCH_ARCH "i686_pc_linux2")
    endif()
  endif()

  # We run cmake -P scripts to download and extract, configure,
  # build, and install this external project.
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_download_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_configure_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_build_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_install_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake @ONLY)

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    DOWNLOAD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_step.cmake
    UPDATE_COMMAND ""
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
    BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake
    INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
