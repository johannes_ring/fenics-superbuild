#
# patch
#

set(proj patch)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})

if (FEniCS_BUILD_${proj})
  set(proj_URL "http://ftp.gnu.org/gnu/patch/patch-2.6.1.tar.gz")
  set(proj_URL_MD5 "d758eb96d3f75047efc004a720d33daf")
  #set(proj_URL "ftp://alpha.gnu.org/gnu/patch/patch-2.6.1.102-5b460.tar.gz")

  # We run cmake -P scripts to configure, build, and
  # install this external project.
  set(ep_additional_configure_args)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_configure_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_build_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_install_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake @ONLY)

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
    BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake
    INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
