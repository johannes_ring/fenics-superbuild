#
# Instant
#

set(proj Instant)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  if (FEniCS_BUILD_STABLE)
    if ("${Instant_VERSION}" VERSION_LESS "1.1")
      set(proj_URL "http://launchpad.net/instant/1.0.x/${proj_VERSION}/+download/instant-${proj_VERSION}.tar.gz")
    elseif ("${Instant_VERSION}" VERSION_LESS "1.2")
      set(proj_URL "http://launchpad.net/instant/1.1.x/${proj_VERSION}/+download/instant-${proj_VERSION}.tar.gz")
    elseif ("${Instant_VERSION}" VERSION_LESS "1.3")
      set(proj_URL "http://launchpad.net/instant/1.2.x/${proj_VERSION}/+download/instant-${proj_VERSION}.tar.gz")
    else()
      set(proj_URL "https://bitbucket.org/fenics-project/instant/downloads/instant-${proj_VERSION}.tar.gz")
    endif()
    set(proj_URL_MD5_0.9.8 "e77edecc222bd2916947f9eca49ca131")
    set(proj_URL_MD5_0.9.9 "df0016423c3ff41ec1a9384c8d3bb36e")
    set(proj_URL_MD5_0.9.10 "1493dcb34205717559c100964e219d2f")
    set(proj_URL_MD5_1.0-beta "d481172899afe5f99de5615938f011d2")
    set(proj_URL_MD5_1.0.0 "b73ffae5f590eeaaf4682e32a44b36cc")
    set(proj_URL_MD5_1.0.1 "c1205de16f70bb412561830f1080dc4e")
    set(proj_URL_MD5_1.1.0 "67986bbc0a4dcb990d6d4981a6605540")
    set(proj_URL_MD5_1.2.0 "36ce7ede977e0dc2e61e756c036ea29b")
    set(proj_URL_MD5_1.3.0 "5c25d2ce48ee761a7113f7a80b25327e")
    set(proj_URL_MD5_1.4.0 "e6fa58eb5df4401aac8542110dc1d220")
    set(proj_URL_MD5_1.5.0 "b744023ded27ee9df4a8d8c6698c0d58")
    set(proj_URL_MD5_1.6.0 "5f2522eb032a5bebbad6597b6fe0732a")
    set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")
  else()
    set(proj_URL "http://fenicsproject.org/pub/software/instant/instant-snapshot.tar.gz")
    set(proj_URL_MD5 "")
  endif()

  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_build_args "--compiler=mingw32")
    set(proj_additional_instant_args "--no-compile")
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )

  if (FEniCS_FORCE_REBUILD)
    externalproject_add_step(${proj} force_rebuild
      COMMENT "Force ${proj} re-build"
      DEPENDERS download
      ALWAYS 1
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
