#
# GLib
#

set(proj GLib)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})

if (FEniCS_BUILD_${proj})
  if (WIN32)
    # We use prebuilt binaries on Windows.
    set(proj_URL "http://ftp.gnome.org/pub/gnome/binaries/win32/glib/2.24/glib_2.24.2-2_win32.zip")
    set(proj_URL_MD5 "e021f2945e324d5753202ad5a9a6b893")
  else()
    set(proj_URL "http://ftp.gnome.org/pub/gnome/sources/glib/2.26/glib-2.26.0.tar.gz")
    set(proj_URL_MD5 "9496ef773f323c3fceadfdcebcaf43c7")
  endif()

  if (MINGW)
    # FIXME: Can we use standard DOWNLOAD_COMMAND on Windows also?
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_download_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_step-mingw.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_install_step-mingw.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step-mingw.cmake @ONLY)

    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      DOWNLOAD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_step-mingw.cmake
      UPDATE_COMMAND ""
      CONFIGURE_COMMAND ""
      BUILD_COMMAND ""
      INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step-mingw.cmake
      )
  else()
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ${ep_source_dir}/${proj}/configure --prefix=${ep_install_dir}
      )
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
