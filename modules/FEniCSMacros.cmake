
#-----------------------------------------------------------------------------
# Convenient macro allowing to download a file
#------------------------------------------------------------------------------

function(downloadFile url dest)
  if (NOT WGET_EXECUTABLE)
    find_program(WGET_EXECUTABLE wget
      NAMES wget wget.exe
      HINTS 
        ${ep_install_dir}/bin
      PATHS
        "C:/Program Files/GnuWin32/bin"
        "C:/Program Files (x86)/GnuWin32/bin"
        "${MSYS_DIR}/bin"
      )
  endif()

  find_program(CURL_COMMAND curl)

  if (WGET_EXECUTABLE)
    set(WGET_additional_args)
    if (NOT WIN32)
      set(WGET_additional_args "--no-check-certificate")
    endif()
    execute_process(
      COMMAND ${WGET_EXECUTABLE} ${WGET_additional_args} --continue --output-document=${dest} ${url}
      RESULT_VARIABLE error_code
      ERROR_VARIABLE error_msg)
  elseif (CURL_COMMAND)
    execute_process(
      COMMAND ${CURL_COMMAND} --insecure --location --output ${dest} ${url}
      RESULT_VARIABLE error_code
      ERROR_VARIABLE error_msg)
  else()
    file(DOWNLOAD ${url} ${dest} STATUS status)
    list(GET status 0 error_code)
    list(GET status 1 error_msg)
  endif()

  if (error_code OR NOT EXISTS ${dest})
    message(FATAL_ERROR "error: Failed to download ${url}")
  endif()
endfunction()

#------------------------------------------------------------------------------
# Convenient macro allowing to define an "empty" project in case an external
# one is provided using for example <proj>_DIR. Doing so allows to keep
# the external project dependency system happy.
#------------------------------------------------------------------------------

macro(FEniCSMacro_Empty_ExternalProject proj dependencies)
  externalproject_add(${proj}
    DEPENDS ${dependencies}
    DOWNLOAD_COMMAND ""
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
    )
endmacro()
