#
# Armadillo
#

set(proj Armadillo)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  set(proj_URL "http://downloads.sourceforge.net/arma/armadillo-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_1.0.4 "e12f6f1cc04f26309ad5e8d845d49093")
  set(proj_URL_MD5_3.2.2 "f1a3b1dab80fb274e71c6c2a2484e647")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  set(Armadillo_LAPACK_args)
  # FIXME: Set path to LAPACK library
  if (DEFINED ATLAS_DIR)
    set(Armadillo_LAPACK_args)
  elseif (DEFINED LAPACK_DIR)
    set(Armadillo_LAPACK_args)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CMAKE_GENERATOR ${CMAKE_GENERATOR}
    CMAKE_ARGS
      ${ep_common_args}
      -D CMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
      -D CMAKE_VERBOSE_MAKEFILE:BOOL=OFF
      -D LIB_INSTALL_DIR=lib
      -D INSTALL_LIB_DIR=lib
      ${Boost_CMAKE_ARGS}
    BUILD_IN_SOURCE ON
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
    fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
