#
# Eigen3
#

set(proj Eigen3)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  set(proj_URL "http://fenicsproject.org/pub/software/contrib/eigen3_${proj_VERSION}.tar.bz2")
  set(proj_URL_MD5_3.2.0 "894381be5be65bb7099c6fd91d61b357")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CMAKE_GENERATOR ${CMAKE_GENERATOR}
    CMAKE_ARGS
      ${ep_common_args}
      #-D CMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
      -D CMAKE_VERBOSE_MAKEFILE:BOOL=OFF
      -D BUILD_TESTING:BOOL=ON
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
    fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
