#
# CGAL
#

set(proj CGAL)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  set(proj_URL_3.6.1 "https://gforge.inria.fr/frs/download.php/27222/CGAL-3.6.1.tar.gz")
  set(proj_URL_3.8 "https://gforge.inria.fr/frs/download.php/28500/CGAL-3.8.tar.gz")
  set(proj_URL_3.9 "https://gforge.inria.fr/frs/download.php/29125/CGAL-3.9.tar.gz")
  set(proj_URL_4.0 "https://gforge.inria.fr/frs/download.php/30387/CGAL-4.0.tar.gz")
  set(proj_URL_4.0.2 "https://gforge.inria.fr/frs/download.php/31175/CGAL-4.0.2.tar.gz")
  set(proj_URL_4.1 "https://gforge.inria.fr/frs/download.php/31641/CGAL-4.1.tar.gz")
  set(proj_URL_4.3 "https://gforge.inria.fr/frs/download.php/32994/CGAL-4.3.tar.gz")
  set(proj_URL "${proj_URL_${proj_VERSION}}")
  set(proj_URL_MD5_3.6.1 "c84e11327bbdd8d83e2abd17c5293b5b")
  set(proj_URL_MD5_3.8 "b8a79e62e4d8ba8b649d815aebbd1c0a")
  set(proj_URL_MD5_3.9 "797697130ff9231627521c0a38f16d2f")
  set(proj_URL_MD5_4.0 "5e0c11a3f3628f58c5948d6d10271f0c")
  set(proj_URL_MD5_4.0.2 "d0d1577f72fc8ab49fed3d966dae19da")
  set(proj_URL_MD5_4.1 "2c1f7aae985084a0b78fc18ab5ad1cb8")
  set(proj_URL_MD5_4.3 "3fa2d43adbf5c05d76c5ec01f5033cc9")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")
  set(proj_EXTRACTSTO "CGAL-${proj_VERSION}")

  # We run a cmake -P script to download and extract this external project
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_download_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_step.cmake @ONLY)

  set(ep_patch_command "")
  if (APPLE AND "${proj_VERSION}" VERSION_EQUAL "3.6.1")
    set(ep_patch_command
      ${PATCH_EXECUTABLE} -p0 --forward --batch < ${CMAKE_CURRENT_SOURCE_DIR}/patches/cgal.patch
      )
  endif()

  set(CMAKE_ARGS
    ${ep_common_args}
    #-D CMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
    -D CMAKE_VERBOSE_MAKEFILE:BOOL=OFF
    -D LIB_INSTALL_DIR:STRING=lib
    -D CGAL_INSTALL_LIB_DIR:STRING=lib
    -D WITH_CGAL_Core:BOOL=OFF
    -D WITH_CGAL_ImageIO:BOOL=OFF
    -D WITH_CGAL_Qt3:BOOL=OFF
    -D WITH_CGAL_Qt4:BOOL=OFF
    -D CGAL_SOURCE_INSTALL:BOOL=OFF
    ${Boost_CMAKE_ARGS}
    )

  if (DEFINED GMP_DIR)
    list(APPEND CMAKE_ARGS
      -D WITH_GMP:BOOL=ON
      -D GMP_DIR:PATH=${GMP_DIR}
      -D GMP_INC_DIR:PATH=${GMP_DIR}/include
      -D GMP_LIB_DIR:PATH=${GMP_DIR}/lib
      -D WITH_GMPXX:BOOL=ON
      -D GMPXX_DIR:PATH=${GMP_DIR}
      -D GMPXX_INC_DIR:PATH=${GMP_DIR}/include
      -D GMPXX_LIB_DIR:PATH=${GMP_DIR}/lib
      )
  endif()

  if (DEFINED MPFR_DIR)
    list(APPEND CMAKE_ARGS
      -D WITH_MPFR:BOOL=ON
      -D MPFR_DIR:PATH=${MPFR_DIR}
      -D MPFR_INC_DIR:PATH=${MPFR_DIR}/include
      -D MPFR_LIB_DIR:PATH=${MPFR_DIR}/lib
      )
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    DOWNLOAD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_step.cmake
    DOWNLOAD_DIR ${ep_download_dir}
    PATCH_COMMAND ${ep_patch_command}
    CMAKE_GENERATOR ${CMAKE_GENERATOR}
    CMAKE_ARGS ${CMAKE_ARGS}
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
    fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
