#
# zlib
#

set(proj zlib)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  set(proj_URL "http://zlib.net/zlib-1.2.5.tar.gz")
  set(proj_URL_MD5 "c735eab2d659a96e5a594c9e8541ad63")

  if (MINGW)
    # We run cmake -P scripts to configure, build, and
    # install this external project.
    set(ep_additional_configure_args)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_configure_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_build_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_install_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake @ONLY)
  
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
      BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake
      INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake
      )
  else()
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ./configure --prefix=${ep_install_dir}
      BUILD_IN_SOURCE ON
      )
  endif()

  set(ZLIB_DIR ${ep_install_dir})
  set(ZLIB_INCLUDE_DIRS ${ZLIB_DIR}/include)
  set(ZLIB_LIBRARY_DIR ${ZLIB_DIR}/lib)
  set(ZLIB_LIBRARY ${ZLIB_LIBRARY_DIR}/libz${CMAKE_SHARED_LIBRARY_SUFFIX})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
