# - Find SCons
#
# This module looks for SCons. The following values will be defined:
#  SCONS_EXECUTABLE: the full path to the SCons build tool.
#  SCONS_FOUND: True if SCons has been found.

find_program(SCONS_EXECUTABLE
  NAMES scons
  DOC "SCons build tool"
  )
mark_as_advanced(SCONS_EXECUTABLE)

# Handle the QUIETLY and REQUIRED arguments and set SCONS_FOUND to TRUE if
# all listed variables are TRUE

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(SCons DEFAULT_MSG SCONS_EXECUTABLE)
