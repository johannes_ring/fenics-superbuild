#
# FErari
#

set(proj FErari)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  if (FEniCS_BUILD_STABLE)
    # FIXME: What should FErari_VERSION be?
    set(FErari_VERSION "0.2.0")
    set(proj_URL "http://launchpad.net/ferari/trunk/${proj_VERSION}/+download/ferari-${proj_VERSION}.tar.gz")
    set(proj_URL_MD5_0.2.0 "581b011631753d54cd915f2068532ee7")
    set(proj_URL_MD5_1.0.0 "e41ed02517f0e8832f27d24e7755c0e2")
    set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")
  else()
    set(FErari_VERSION "snapshot")
    set(proj_URL "http://www.fenicsproject.org/pub/software/ferari/ferari-snapshot.tar.gz")
    set(proj_URL_MD5 "")
  endif()

  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_build_args "--compiler=mingw32")
    set(proj_additional_install_args "--no-compile")
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )

  if (FEniCS_FORCE_REBUILD)
    externalproject_add_step(${proj} force_rebuild
      COMMENT "Force ${proj} re-build"
      DEPENDERS download
      ALWAYS 1
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
