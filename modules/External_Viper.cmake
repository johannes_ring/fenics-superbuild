#
# Viper
#

set(proj Viper)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  if (FEniCS_BUILD_STABLE)
    set(proj_URL "http://launchpad.net/fenics-viper/1.0.x/${proj_VERSION}/+download/viper-${proj_VERSION}.tar.gz")
    set(proj_URL_MD5_0.4.6 "2ad5a87ff2410d41a35a09ddc3d43836")
    set(proj_URL_MD5_0.4.7 "6aca08bb60f0530a8ea6840ca437f0ec")
    set(proj_URL_MD5_1.0-beta "7cc1e4fe7848c52c8b651fcd910e230b")
    set(proj_URL_MD5_1.0.0 "4aa7d37934c98e637b45eb9cc7d03cdd")
    set(proj_URL_MD5_1.0.1 "6c4564f67c4b5f8c01425ca3d2cbd2a3")
    set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")
  else()
    set(proj_URL "http://fenicsproject.org/pub/software/viper/viper-snapshot.tar.gz")
    set(proj_URL_MD5 "")
  endif()

  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_build_args "--compiler=mingw32")
    set(proj_additional_install_args "--no-compile")
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )

  if (FEniCS_FORCE_REBUILD)
    externalproject_add_step(${proj} force_rebuild
      COMMENT "Force ${proj} re-build"
      DEPENDERS download
      ALWAYS 1
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
