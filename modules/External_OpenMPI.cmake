#
# OpenMPI
#

set(proj OpenMPI)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  if ("${proj_VERSION}" VERSION_EQUAL "1.6")
    set(proj_URL "http://www.open-mpi.org/software/ompi/v1.6/downloads/openmpi-1.6.tar.gz")
    set(proj_URL_MD5 "3ed0c892a0c921270cb9c7af2fdfd2d2")
  elseif ("${proj_VERSION}" VERSION_EQUAL "1.5")
    set(proj_URL "http://www.open-mpi.org/software/ompi/v1.5/downloads/openmpi-1.5.tar.gz")
    set(proj_URL_MD5 "6a530b991e3a092af4408524569bb9e6")
  else()
    set(proj_URL "http://www.open-mpi.org/software/ompi/v1.6/downloads/openmpi-1.6.2.tar.gz")
    set(proj_URL_MD5 "351845a0edd8141feb30d31edd59cdcd")
  endif()

  set(ep_additional_configure_args
    --enable-mpi-thread-multiple
    --enable-opal-multi-threads
    --with-threads=posix
    --disable-mpi-profile
    )

  if (APPLE)
    set(ep_additional_configure_args
      ${ep_additional_configure_args}
      --disable-mpi-f90
      )
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${ep_source_dir}/${proj}/configure --prefix=${ep_install_dir} ${ep_additional_configure_args}
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
