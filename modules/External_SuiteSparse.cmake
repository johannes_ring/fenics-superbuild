#
# SuiteSparse
#

set(proj SuiteSparse)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  # Use a SuiteSparse version that is compatible with PETSc:
  # SuiteSparse 3.1.0 is compatible with PETSc 3.1
  # SuiteSparse 3.6.1 is compatible with PETSc 3.2
  set(proj_URL "http://faculty.cse.tamu.edu/davis/SuiteSparse/SuiteSparse-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_3.1.0 "58d90444feef92fc7c265cbd11a757c6")
  set(proj_URL_MD5_3.4.0 "e59dcabc9173b1ba1b3659ae147006b8")
  set(proj_URL_MD5_3.6.1 "88a44890e8f61cdbb844a76b7259d876")
  set(proj_URL_MD5_3.7.1 "18f539f4927dc0ed6991348cf6ba8f41")
  set(proj_URL_MD5_4.0.2 "efe53b7ef2b529d54727815231a5a6a9")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  if (NOT DEFINED LAPACK_DIR)
    set(LAPACK_DIR "/usr")
  endif()

  # We run cmake -P scripts to configure, build, and
  # install this external project.
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_configure_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_build_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_install_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake @ONLY)

  set(proj_PATCH_COMMAND "")
  if ("${proj_VERSION}" VERSION_EQUAL "3.7.1")
    set(proj_PATCH_COMMAND ${PATCH_EXECUTABLE} -p1 --forward --batch < ${CMAKE_CURRENT_SOURCE_DIR}/patches/suitesparse-3.7.1-disable-umfpack-demos.patch)
  elseif ("${proj_VERSION}" VERSION_EQUAL "3.6.1")
    set(proj_PATCH_COMMAND ${PATCH_EXECUTABLE} -p1 --forward --batch < ${CMAKE_CURRENT_SOURCE_DIR}/patches/suitesparse-3.6.1-disable-umfpack-demos.patch)
  else()
    if (APPLE)
      set(proj_PATCH_COMMAND
	${PATCH_EXECUTABLE} -p1 --forward --batch < ${CMAKE_CURRENT_SOURCE_DIR}/patches/suitesparse-disable-umfpack-demos.patch)
    endif()
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    PATCH_COMMAND ${proj_PATCH_COMMAND}
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
    BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake
    INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake
    )

  if (MINGW)
    # We need to build shared libraries, run a cmake -P script
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_build_shared_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_shared_step.cmake @ONLY)
    
    externalproject_add_step(${proj} build_shared
      COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_shared_step.cmake
      COMMENT "Building shared libraries for '${proj}'"
      DEPENDEES build
      DEPENDERS install
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )
  endif()

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
