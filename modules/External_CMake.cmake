#
# CMake
#

set(proj CMake)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  string(REGEX REPLACE "^([0-9]+).*$" "\\1" CMake_VERSION_MAJOR "${proj_VERSION}")
  string(REGEX REPLACE "^[0-9]+\\.([0-9]+).*$" "\\1" CMake_VERSION_MINOR "${proj_VERSION}")
  string(REGEX REPLACE "^[0-9]+\\.[0-9]+\\.([0-9]+).*$" "\\1" CMake_VERSION_PATCH "${proj_VERSION}")

  set(proj_URL "http://www.cmake.org/files/v${CMake_VERSION_MAJOR}.${CMake_VERSION_MINOR}/cmake-${CMake_VERSION}.tar.gz")
  set(proj_URL_MD5_2.8.2 "8c967d5264657a798f22ee23976ff0d9")
  set(proj_URL_MD5_2.8.6 "2147da452fd9212bb9b4542a9eee9d5b")
  set(proj_URL_MD5_2.8.8 "ba74b22c788a0c8547976b880cd02b17")
  set(proj_URL_MD5_2.8.12 "105bc6d21cc2e9b6aff901e43c53afea")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  set(proj_patch_command "")
  if (MINGW)
    set(proj_patch_command ${PATCH_EXECUTABLE} -p1 --forward --batch < ${CMAKE_CURRENT_SOURCE_DIR}/patches/cmake-mingw-default-generator.patch)
  elseif (APPLE)
    set(proj_patch_command ${PATCH_EXECUTABLE} -p1 --forward --batch < ${CMAKE_CURRENT_SOURCE_DIR}/patches/cmake_no_macports.patch)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    PATCH_COMMAND ${proj_patch_command}
    DOWNLOAD_DIR ${ep_download_dir}
    CMAKE_GENERATOR ${CMAKE_GENERATOR}
    CMAKE_ARGS
      ${ep_common_args}
      -D CMAKE_VERBOSE_MAKEFILE:BOOL=OFF
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
