#
# FIAT
#

set(proj FIAT)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  if (FEniCS_BUILD_STABLE)
    if ("${FIAT_VERSION}" VERSION_EQUAL "1.1")
      set(proj_URL "http://launchpad.net/fiat/1.1.x/release-${proj_VERSION}/+download/fiat-${proj_VERSION}.tar.gz")
    elseif ("${FIAT_VERSION}" VERSION_LESS "1.3.0")
      set(proj_URL "http://launchpad.net/fiat/1.0.x/${proj_VERSION}/+download/fiat-${proj_VERSION}.tar.gz")
    else()
      set(proj_URL "https://bitbucket.org/fenics-project/fiat/downloads/fiat-${proj_VERSION}.tar.gz")
    endif()
    set(proj_URL_MD5_0.9.2 "42178e666b264df9d0ab63ecddbee8ec")
    set(proj_URL_MD5_0.9.9 "62fd06a9a28537b4f73a98f345f91db6")
    set(proj_URL_MD5_1.0-beta "b7a619aa6c666b9844554709c8611222")
    set(proj_URL_MD5_1.0.0 "fcbad66fb59a07b716e072a15fc51bb5")
    set(proj_URL_MD5_1.1 "87d0919e74fb6539df0f310a3a0560d3")
    set(proj_URL_MD5_1.3.0 "e12267262b97a40fea97c0dad9804dd7")
    set(proj_URL_MD5_1.4.0 "da9c38cf394594091ec4ac2ff652e170")
    set(proj_URL_MD5_1.5.0 "da3fa4dd8177bb251e7f68ec9c7cf6c5")
    set(proj_URL_MD5_1.6.0 "f4509d05c911fd93cea8d288a78a6c6f")
    set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")
  else()
    set(proj_URL "http://fenicsproject.org/pub/software/fiat/fiat-snapshot.tar.gz")
    set(proj_URL_MD5 "")
  endif()

  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_build_args "--compiler=mingw32")
    set(proj_additional_install_args "--no-compile")
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )

  if (FEniCS_FORCE_REBUILD)
    externalproject_add_step(${proj} force_rebuild
      COMMENT "Force ${proj} re-build"
      DEPENDERS download
      ALWAYS 1
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
