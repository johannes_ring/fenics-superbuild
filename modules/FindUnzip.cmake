# - Find unzip
#
# This module looks for unzip. The following values will be defined:
#  UNZIP_EXECUTABLE: the full path to the unzip tool.
#  UNZIP_FOUND: True if unzip has been found.

include(FindCygwin)
include(FindMSYS)

find_program(UNZIP_EXECUTABLE
  NAMES unzip
  PATHS
    "C:/Program Files/GnuWin32/bin"
    "C:/Program Files (x86)/GnuWin32/bin"
    "${MSYS_INSTALL_PATH}/bin"
    "${CYGWIN_INSTALL_PATH}/bin"
  DOC "unzip command for extracting files from ZIP archive"
  )
mark_as_advanced(UNZIP_EXECUTABLE)

# Handle the QUIETLY and REQUIRED arguments and set UNZIP_FOUND to TRUE if
# all listed variables are TRUE

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Unzip DEFAULT_MSG UNZIP_EXECUTABLE)
