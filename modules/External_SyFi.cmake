#
# SyFi
#

set(proj SyFi)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
string(TOUPPER ${proj} proj_UPPER)

if (FEniCS_BUILD_${proj})
  if (FEniCS_BUILD_STABLE)
    set(SyFi_VERSION "0.6.2")
    set(proj_URL "http://launchpad.net/fenics-syfi/trunk/${SyFi_VERSION}/+download/syfi-${SyFi_VERSION}.tar.gz")
    set(proj_URL_MD5 "174c33cd9476bdfc25a7f67242152732")
  else()
    set(SyFi_VERSION "snapshot")
    set(proj_URL "http://www.fenicsproject.org/pub/software/syfi/syfi-snapshot.tar.gz")
    set(proj_URL_MD5 "")
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${SCONS_EXECUTABLE}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${SCONS_EXECUTABLE} install prefix=${ep_install_dir}
    )

  if (FEniCS_FORCE_REBUILD)
    externalproject_add_step(${proj} force_rebuild
      COMMENT "Force ${proj} re-build"
      DEPENDERS download
      ALWAYS 1
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
