#
# Environment Modules
#

set(proj EnvironmentModules)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  set(proj_URL "http://sourceforge.net/projects/modules/files/Modules/modules-${proj_VERSION}/modules-${proj_VERSION}c.tar.gz")
  set(proj_URL_MD5 "7268104661f6a4b63ea17e906ed260b4")

  set(ep_configure_args
    --prefix=${ep_install_dir}
    --with-module-path=${ep_install_dir}
    )

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${ep_source_dir}/${proj}/configure ${ep_configure_args}
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
  set(EnvironmentModulesCMake ${ep_install_dir}/Modules/${proj_VERSION}/init/cmake)
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
