#
# PCRE
#

set(proj PCRE)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

if (FEniCS_BUILD_${proj})
  set(proj_URL "http://downloads.sourceforge.net/project/pcre/pcre/${proj_VERSION}/pcre-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_8.13 "b40adc98dcc2fa2eb3d5edcb6e5f2225")
  set(proj_URL_MD5_8.30 "d5ee0d9f6d2f0b7489331d04b6c182ef")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  if (MINGW)
    set(ep_additional_configure_args)
    if (ep_build_shared_libs)
      set(ep_additional_configure_args "--enable-shared --disable-static")
    endif()

    # We run cmake -P scripts to configure, build, and
    # install this external project.
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_configure_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_build_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_install_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake @ONLY)
  
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
      BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake
      INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake
      )
  else()
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ${ep_source_dir}/${proj}/configure --prefix=${ep_install_dir}
        ${ep_additional_configure_args}
      )
  endif()

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
