# - Find wget
#
# This module looks for wget. The following values will be defined:
#  WGET_EXECUTABLE: the full path to the wget tool.
#  WGET_FOUND: True if wget has been found.

include(FindCygwin)
include(FindMSYS)

find_program(WGET_EXECUTABLE
  NAMES wget
  PATHS
    "C:/Program Files/GnuWin32/bin"
    "C:/Program Files (x86)/GnuWin32/bin"
    "${MSYS_INSTALL_PATH}/bin"
    "${CYGWIN_INSTALL_PATH}/bin"
  DOC "wget command for downloading files"
  )
mark_as_advanced(WGET_EXECUTABLE)

# Handle the QUIETLY and REQUIRED arguments and set WGET_FOUND to TRUE if
# all listed variables are TRUE

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Wget DEFAULT_MSG WGET_EXECUTABLE)
