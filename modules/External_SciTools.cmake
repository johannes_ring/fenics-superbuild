#
# SciTools
#

set(proj SciTools)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  set(proj_URL "http://scitools.googlecode.com/files/scitools-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_0.7 "cda9d7e936d2f12c44d539afd633b0c2")
  set(proj_URL_MD5_0.8 "8b8d0cc938616ef444f6b3cdb8cd26a8")
  set(proj_URL_MD5_0.9.0 "731e7ef3a708e2222f32cba726bfdb68")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  set(proj_additional_configure_args)
  set(proj_additional_build_args --easyviz_backend vtk)
  set(proj_additional_install_args --easyviz_backend vtk)

  if (MINGW)
    set(proj_additional_install_args ${proj_additional_install_args} --no-compile)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
