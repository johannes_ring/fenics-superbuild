#
# LAPACK
#

set(proj LAPACK)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  set(proj_URL "http://www.netlib.org/lapack/lapack-3.2.1-CMAKE.tgz")
  set(proj_URL_MD5 "71ce47837af7d3ba44aa04c6e74e53ff")

  if (MINGW)
    # We run cmake -P scripts to configure, build, and
    # install this external project.
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_configure_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/LAPACK_build_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/LAPACK_build_step.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/LAPACK_install_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/LAPACK_install_step.cmake @ONLY)

    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/LAPACK_configure_step.cmake
      BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/LAPACK_build_step.cmake
      INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/LAPACK_install_step.cmake
      )
  else()
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CMAKE_GENERATOR ${CMAKE_GENERATOR}
      CMAKE_ARGS
        ${ep_common_args}
        -D CMAKE_VERBOSE_MAKEFILE:BOOL=ON
        -D CMAKE_Fortran_USE_RESPONSE_FILE_FOR_OBJECTS:BOOL=ON
      INSTALL_COMMAND ""
      )

    externalproject_add_step(${proj} install_blas_lib
      COMMAND ${CMAKE_COMMAND} -E copy BLAS/SRC/libblas${CMAKE_SHARED_LIBRARY_SUFFIX} ${ep_install_dir}/lib/
      COMMENT "Installing BLAS library for '${proj}'"
      DEPENDEES install
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )

    externalproject_add_step(${proj} install_lapack_lib
      COMMAND ${CMAKE_COMMAND} -E copy SRC/liblapack${CMAKE_SHARED_LIBRARY_SUFFIX} ${ep_install_dir}/lib/
      COMMENT "Installing LAPACK library for '${proj}'"
      DEPENDEES install
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )
  endif()

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
