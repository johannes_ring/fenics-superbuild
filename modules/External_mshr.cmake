#
# mshr
#

set(proj mshr)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  if (FEniCS_BUILD_STABLE)
    set(proj_URL "https://bitbucket.org/fenics-project/mshr/downloads/mshr-${proj_VERSION}.tar.gz")
    set(proj_URL_MD5_1.5.0 "fa88d48760686e9cbb3b468d3f362f77")
    set(proj_URL_MD5_1.6.0 "44160cccb8a006be27c8d9c33af6a397")
    set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")
  else()
    set(proj_URL "http://fenicsproject.org/pub/software/mshr/mshr-snapshot.tar.gz")
    set(proj_URL_MD5 "")
  endif()

  set(extra_cmake_args "")
  if (DEFINED GMP_DIR)
    list(APPEND extra_cmake_args
      -D GMP_DIR:PATH=${GMP_DIR}
      -D GMP_INC_DIR:PATH=${GMP_DIR}/include
      -D GMP_LIB_DIR:PATH=${GMP_DIR}/lib
      -D GMPXX_DIR:PATH=${GMP_DIR}
      -D GMPXX_INC_DIR:PATH=${GMP_DIR}/include
      -D GMPXX_LIB_DIR:PATH=${GMP_DIR}/lib
      )
    set(ENV{GMP_DIR} ${GMP_DIR})
    set(ENV{GMP_INC_DIR} ${GMP_DIR}/include)
    set(ENV{GMP_LIB_DIR} ${GMP_DIR}/lib)
  endif()

  if (DEFINED MPFR_DIR)
    list(APPEND extra_cmake_args
      -D MPFR_DIR:PATH=${MPFR_DIR}
      -D MPFR_INC_DIR:PATH=${MPFR_DIR}/include
      -D MPFR_LIB_DIR:PATH=${MPFR_DIR}/lib
      )
    set(ENV{MPFR_DIR} ${MPFR_DIR})
    set(ENV{MPFR_INC_DIR} ${MPFR_DIR}/include)
    set(ENV{MPFR_LIB_DIR} ${MPFR_DIR}/lib)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CMAKE_GENERATOR ${CMAKE_GENERATOR}
    CMAKE_ARGS
      ${ep_common_args}
      ${ep_additional_args}
      #-D CMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
      -D CMAKE_VERBOSE_MAKEFILE:BOOL=OFF
      -D SWIG_EXECUTABLE:FILEPATH=${SWIG_EXECUTABLE}
      -D CMAKE_SKIP_RPATH:BOOL=ON
      -D CMAKE_INSTALL_RPATH_USE_LINK_PATH:BOOL=OFF
      ${Boost_CMAKE_ARGS}
      ${extra_cmake_args}
    )

  if (FEniCS_FORCE_REBUILD)
    externalproject_add_step(${proj} force_rebuild
      COMMENT "Force ${proj} re-build"
      DEPENDERS download
      ALWAYS 1
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
