#
# IPython
#

set(proj IPython)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  set(proj_URL "http://archive.ipython.org/release/${proj_VERSION}/ipython-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_0.10.1 "54ae47079b0e9a0998593a99ce76ec1f")
  set(proj_URL_MD5_0.11 "efc899e752a4a4a67a99575cea1719ef")
  set(proj_URL_MD5_1.1.0 "70d69c78122923879232567ac3c47cef")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  set(proj_additional_configure_args)
  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_configure_args "--compiler=mingw32")
    set(proj_additional_build_args "--compiler=mingw32")
    set(proj_additional_install_args "--no-compile")
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )

  if (WIN32)
    # We add an additional step to install some batch files
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_install_batch_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_batch_step.cmake @ONLY)

    externalproject_add_step(${proj} install_batch
      COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_batch_step.cmake
      COMMENT "Installing batch files for '${proj}'"
      DEPENDEES install
      WORKING_DIRECTORY ${ep_source_dir}/${proj}
      )
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
