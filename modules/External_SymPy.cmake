#
# SymPy
#

set(proj SymPy)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  set(proj_URL "https://github.com/sympy/sympy/releases/download/sympy-${proj_VERSION}/sympy-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_0.7.6 "3d04753974306d8a13830008e17babca")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  set(proj_additional_configure_args)
  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_install_args ${proj_additional_install_args} --no-compile)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
