#
# HDF5
#

set(proj HDF5)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  set(proj_URL "http://www.hdfgroup.org/ftp/HDF5/releases/hdf5-${proj_VERSION}/src/hdf5-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_1.8.9 "d1266bb7416ef089400a15cc7c963218")
  set(proj_URL_MD5_1.8.10 "710aa9fb61a51d61a7e2c09bf0052157")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  set(configure_args
    --prefix=${ep_install_dir}
    --with-pthread
    --enable-unsupported
    --enable-shared
    --enable-production=yes
    --disable-sharedlib-rpath
    --enable-parallel=yes
    --enable-largefile=yes
    --with-default-api-version=v18
    CC=mpicc
    )

  if (DEFINED ZLIB_DIR)
    list(APPEND configure_args --with-zlib=${ZLIB_DIR})
  endif()

  if (CMAKE_Fortran_COMPILER_WORKS)
    list(APPEND configure_args --enable-fortran=yes FC=mpif90 F9X=mpif90)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${ep_source_dir}/${proj}/configure ${configure_args}
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
