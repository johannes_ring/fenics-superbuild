#
# SLEPc
#

set(proj SLEPc)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  #set(proj_URL "http://www.grycap.upv.es/slepc/download/distrib/slepc-${proj_VERSION}.tgz")
  set(proj_URL "http://www.grycap.upv.es/slepc/download/distrib/slepc-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_3.1-p6 "a57f717fa52d3a1fedd98137ca0325a3")
  set(proj_URL_MD5_3.2-p5 "8d2252677cc326ecdcb90cd9039920cf")
  set(proj_URL_MD5_3.3-p3 "c6a9d7fe2f6eaff0ac65854d44c0bb57")
  set(proj_URL_MD5_3.4.4 "329179c5139027c7b293dd5ab0bc112c")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  if ("${proj_VERSION}" VERSION_GREATER "3.4")
    set(PATCH_COMMAND "")
  elseif ("${proj_VERSION}" VERSION_LESS "3.2")
    set(PATCH_COMMAND ${PATCH_EXECUTABLE} -p1 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/slepc-3.1.patch)
  else()
    set(PATCH_COMMAND ${PATCH_EXECUTABLE} -p1 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/slepc-3.2.patch)
  endif()

  # Set PETSC_ARCH if not already defined
  if (NOT DEFINED PETSC_ARCH)
    if (APPLE)
      set(PETSC_ARCH "darwin${DARWIN_MAJOR_VERSION}.0.0-cxx-opt")
    else()
      set(PETSC_ARCH "linux-gnu-cxx-opt")
    endif()
  endif()

  # Set PETSC_DIR to ep_install_dir if not already defined
  if (NOT DEFINED PETSC_DIR)
    set(PETSC_DIR ${ep_install_dir})
    message(WARNING "PETSC_DIR not set. Trying with PETSC_DIR=${PETSC_DIR}.")
  endif()

  # We run cmake -P scripts to configure, build, and
  # install this external project.
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_configure_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_build_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_install_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake @ONLY)

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    PATCH_COMMAND ${PATCH_COMMAND}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
    BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake
    INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
