# - Find patch
#
# This module looks for patch. The following values will be defined:
#  PATCH_EXECUTABLE: the full path to the patch tool.
#  PATCH_FOUND: True if patch has been found.

include(FindCygwin)
include(FindMSYS)

find_program(PATCH_EXECUTABLE
  NAMES patch
  PATHS
    "C:/Program Files/GnuWin32/bin"
    "C:/Program Files (x86)/GnuWin32/bin"
    "${MSYS_INSTALL_PATH}/bin"
    "${CYGWIN_INSTALL_PATH}/bin"
  DOC "patch command for applying diff file to an original"
  )
mark_as_advanced(PATCH_EXECUTABLE)

# Handle the QUIETLY and REQUIRED arguments and set PATCH_FOUND to TRUE if
# all listed variables are TRUE

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Patch DEFAULT_MSG PATCH_EXECUTABLE)
