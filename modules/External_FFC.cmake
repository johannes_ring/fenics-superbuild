#
# FFC
#

set(proj FFC)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  if (FEniCS_BUILD_STABLE)
    if ("${FFC_VERSION}" VERSION_LESS "1.1")
      set(proj_URL "http://launchpad.net/ffc/1.0.x/${proj_VERSION}/+download/ffc-${proj_VERSION}.tar.gz")
    elseif ("${FFC_VERSION}" VERSION_LESS "1.2")
      set(proj_URL "http://launchpad.net/ffc/1.1.x/${proj_VERSION}/+download/ffc-${proj_VERSION}.tar.gz")
    elseif ("${FFC_VERSION}" VERSION_LESS "1.3")
      set(proj_URL "http://launchpad.net/ffc/1.2.x/${proj_VERSION}/+download/ffc-${proj_VERSION}.tar.gz")
    else()
      set(proj_URL "https://bitbucket.org/fenics-project/ffc/downloads/ffc-${proj_VERSION}.tar.gz")
    endif()
    set(proj_URL_MD5_0.9.4 "be4e74e62e84dc68eb7da2c0ff33ea93")
    set(proj_URL_MD5_0.9.9 "9106fef4ae8a5a4990ce4783cbde7994")
    set(proj_URL_MD5_0.9.10 "2e8f6689de38604e9594f1ef798f207c")
    set(proj_URL_MD5_1.0-beta "d279e786cfa12da8cf818330d05748c2")
    set(proj_URL_MD5_1.0-beta2 "6cdf79884a824ac2636620fc5f6a24ce")
    set(proj_URL_MD5_1.0-rc1 "fa28eb611e2c50e378f31ba61d91f69d")
    set(proj_URL_MD5_1.0.0 "e4838b6eb2a53ba6e0082644e74570aa")
    set(proj_URL_MD5_1.0.1 "46f25c28e86d31dc9157a02aebd77397")
    set(proj_URL_MD5_1.1.0 "f89ed9c48731f6f085fd80714e300da1")
    set(proj_URL_MD5_1.2.0 "50e5d52d9b32249125b030399413b742")
    set(proj_URL_MD5_1.3.0 "d57f736ff0b756935a5ffd7c8b30247b")
    set(proj_URL_MD5_1.4.0 "cac802fbbf869911fdd82b72c51a7771")
    set(proj_URL_MD5_1.5.0 "343f6d30e7e77d329a400fd8e73e0b63")
    set(proj_URL_MD5_1.6.0 "358faa3e9da62a1b1a717070217b793e")
    set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")
  else()
    set(proj_URL "http://fenicsproject.org/pub/software/ffc/ffc-snapshot.tar.gz")
    set(proj_URL_MD5 "")
  endif()

  set(PATCH_COMMAND "")
  if ("${proj_VERSION}" STREQUAL "snapshot")
    set(PATCH_COMMAND "")
  elseif ("${proj_VERSION}" VERSION_EQUAL "0.9.10")
    set(PATCH_COMMAND
      ${PATCH_EXECUTABLE} -p1 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/ffc_0.9.10.patch)
  elseif ("${proj_VERSION}" VERSION_LESS "0.9.10")
    set(PATCH_COMMAND
      ${PATCH_EXECUTABLE} -p0 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/ffc_windows_bugfix.patch)
  endif()

  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_build_args "--compiler=mingw32")
    set(proj_additional_install_args "--no-compile")
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    PATCH_COMMAND ${PATCH_COMMAND}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )

  if (FEniCS_FORCE_REBUILD)
    externalproject_add_step(${proj} force_rebuild
      COMMENT "Force ${proj} re-build"
      DEPENDERS download
      ALWAYS 1
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
