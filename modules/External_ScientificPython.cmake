#
# ScientificPython
#

set(proj ScientificPython)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})

if (FEniCS_BUILD_${proj})
  set(proj_URL "http://sourcesup.cru.fr/frs/download.php/2309/ScientificPython-2.8.tar.gz")
  set(proj_URL_MD5 "82d8592635d6ae8608b3073dacf9e694")
  set(proj_EXTRACTSTO "ScientificPython-2.8")

  # We run a cmake -P script to download and extract this external project
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_download_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_step.cmake @ONLY)

  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    # Note: If gcc complains about -mno-cygwin, remove that flag from distutils.
    set(proj_additional_config_args "--compiler=mingw32")
    set(proj_additional_build_args "--compiler=mingw32")
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    DOWNLOAD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_step.cmake
    PATCH_COMMAND ${PATCH_EXECUTABLE} --forward --batch --binary < ${CMAKE_CURRENT_SOURCE_DIR}/patches/scientificpython.patch
    CONFIGURE_COMMAND ${PYTHON_EXECUTABLE} setup.py config ${proj_additional_config_args}
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
