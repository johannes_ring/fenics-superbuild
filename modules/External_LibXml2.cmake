#
# LibXml2
#

set(proj LibXml2)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

if (FEniCS_BUILD_${proj})
  set(proj_URL "ftp://xmlsoft.org/libxml2/libxml2-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_2.7.7 "9abc9959823ca9ff904f1fbcf21df066")
  set(proj_URL_MD5_2.8.0 "c62106f02ee00b6437f0fb9d370c1093")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  # FIXME: Should this be enabled again?
  set(ep_additional_configure_args)
  if (DEFINED ZLIB_DIR)
    set(ep_additional_configure_args ${ep_additional_configure_args} --with-zlib=${ZLIB_DIR})
  endif()

  if (MINGW)
    # FIXME: Disable threads since it fails to compile testThreads.c.
    # FIXME: This could be a bug since I would expect testThreadsWin32.c
    # FIXME: to be used instead on Windows.
    set(ep_additional_configure_args
      ${ep_additional_configure_args} --with-threads=no)

    # We run cmake -P scripts to configure, build, and
    # install this external project.
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_configure_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_build_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_install_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake @ONLY)
  
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
      BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake
      INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake
      )
  else()
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ${ep_source_dir}/${proj}/configure --prefix=${ep_install_dir} ${ep_additional_configure_args}
      )
  endif()

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
