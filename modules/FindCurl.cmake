# - Find curl
#
# This module looks for curl. The following values will be defined:
#  CURL_EXECUTABLE: the full path to the curl tool.
#  CURL_FOUND: True if curl has been found.

include(FindCygwin)
include(FindMSYS)

find_program(CURL_EXECUTABLE
  NAMES curl
  PATHS
    "C:/Program Files/GnuWin32/bin"
    "C:/Program Files (x86)/GnuWin32/bin"
    "${MSYS_INSTALL_PATH}/bin"
    "${CYGWIN_INSTALL_PATH}/bin"
  DOC "curl command for downloading files"
  )
mark_as_advanced(CURL_EXECUTABLE)

# Handle the QUIETLY and REQUIRED arguments and set CURL_FOUND to TRUE if
# all listed variables are TRUE

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Curl DEFAULT_MSG CURL_EXECUTABLE)
