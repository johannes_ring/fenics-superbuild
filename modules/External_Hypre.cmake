#
# Hypre
#

set(proj Hypre)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if (${proj_UPPER}_EXECUTABLE AND NOT EXISTS ${${proj_UPPER}_EXECUTABLE})
  message(FATAL_ERROR "${proj_UPPER}_EXECUTABLE variable is defined but it corresponds to a non-existing executable.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_EXECUTABLE)
  set(proj_URL "https://computation.llnl.gov/casc/hypre/download/hypre-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_2.6.0b "84381005bdddff69b62b43ca025070fd")
  set(proj_URL_MD5_2.8.0b "6b4db576c68d2072e48efbc00ea58489")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")
  set(proj_EXTRACTSTO "hypre-${proj_VERSION}")

  set(ep_additional_configure_args
    --enable-shared
    --with-blas-libs=
    --with-blas-lib-dir=
    --with-lapack-libs=
    --with-lapack-lib-dir=
    --with-blas=yes
    --with-lapack=yes
    --with-babel=no
    --with-mli=no
    --with-fei=no
    --with-superlu=no
    )

  ## Use 64 bit integers (long long) on 64 bit arches
  #if (CMAKE_SIZEOF_VOID_P EQUAL 8)
  #  set(ep_additional_configure_args ${ep_additional_configure_args} --enable-bigint)
  #endif()

  if (DEFINED OPENMPI_DIR)
    set(ep_additional_configure_args ${ep_additional_configure_args}
      --with-mpi=yes
      --with-mpi-include=${OPENMPI_DIR}/include
      --with-mpi-lib-dirs=${OPENMPI_DIR}/lib
      --with-mpi-libs="mpi"
      #--with-CC=mpicc
      #--with-CXX=mpicxx
      #--with-F77=mpif77
      )
  else()
    set(ep_additional_configure_args ${ep_additional_configure_args} --with-MPI=no)
  endif()

  if (NOT WIN32)
    set(ep_additional_configure_args
      ${ep_additional_configure_args}
      CFLAGS=-fPIC CXXFLAGS=-fPIC FFLAGS=-fPIC LDFLAGS=-fPIC)
  endif()

  # We run cmake -P scripts to download, configure, build, and
  # install this external project.
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_download_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_configure_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_build_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_install_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake @ONLY)

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    DOWNLOAD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_step.cmake
    UPDATE_COMMAND ""
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
    BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake
    INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
