#
# ATLAS
#

set(proj ATLAS)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  #set(proj_URL "http://sourceforge.net/projects/math-atlas/files/Stable/${proj_VERSION}/atlas${proj_VERSION}.tar.bz2")
  #set(proj_URL_MD5 "2030aa079b8d040b93de7018eae90e2b")
  set(proj_URL "http://fenicsproject.org/pub/software/contrib/atlas3.10.0-with-lapack-3.4.2.tar.bz2")
  set(proj_URL_MD5 "ea5ddeb88a55a52cd973db92f56b115d")

  set(ep_additional_configure_args
    --cc="${CMAKE_C_COMPILER}"
    -v 2
    #-D c -DPentiumCPS=<your Mhz>
    -D c -DWALL
    --shared
    --with-netlib-lapack-tarfile=${ep_source_dir}/ATLAS/lapack-3.4.2.tgz
    )

  if (CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(ep_additional_configure_args ${ep_additional_configure_args} -b 64)
  else()
    set(ep_additional_configure_args ${ep_additional_configure_args} -b 32)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${ep_source_dir}/${proj}/configure --prefix=${ep_install_dir} ${ep_additional_configure_args}
    )

  ## FIXME: not tested on Windows or OS X
  #externalproject_add_step(${proj} build_shared
  #  COMMAND ${CMAKE_MAKE_COMMAND} shared
  #  COMMENT "Building shared versions of ATLAS's sequential libs"
  #  DEPENDEES build
  #  DEPENDERS install
  #  WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
  #  )
  #
  #externalproject_add_step(${proj} build_ptshared
  #  COMMAND ${CMAKE_MAKE_COMMAND} ptshared
  #  COMMENT "Building shared versions of ATLAS's threaded libs"
  #  DEPENDEES build
  #  DEPENDERS install
  #  WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
  #  )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
