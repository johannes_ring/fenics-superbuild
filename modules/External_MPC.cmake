#
# MPC
#

set(proj MPC)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  set(proj_URL "ftp://ftp.gnu.org/gnu/mpc/mpc-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_1.0.1 "b32a2e1a3daa392372fbd586d1ed3679")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  set(ep_additional_configure_args --enable-shared)

  if (DEFINED GMP_DIR)
    set(ep_additional_configure_args ${ep_additional_configure_args} --with-gmp=${GMP_DIR})
  endif()

  if (DEFINED MPFR_DIR)
    set(ep_additional_configure_args ${ep_additional_configure_args} --with-mpfr=${MPFR_DIR})
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${ep_source_dir}/${proj}/configure --prefix=${ep_install_dir} ${ep_additional_configure_args}
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
