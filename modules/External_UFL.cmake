#
# UFL
#

set(proj UFL)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})

if (FEniCS_BUILD_${proj})
  if (FEniCS_BUILD_STABLE)
    if ("${UFL_VERSION}" VERSION_LESS "1.1.0")
      set(proj_URL "http://launchpad.net/ufl/1.0.x/${proj_VERSION}/+download/ufl-${proj_VERSION}.tar.gz")
    elseif ("${UFL_VERSION}" VERSION_LESS "1.2.0")
      set(proj_URL "http://launchpad.net/ufl/1.1.x/${proj_VERSION}/+download/ufl-${proj_VERSION}.tar.gz")
    elseif ("${UFL_VERSION}" VERSION_LESS "1.3.0")
      set(proj_URL "http://launchpad.net/ufl/1.2.x/${proj_VERSION}/+download/ufl-${proj_VERSION}.tar.gz")
    else()
      set(proj_URL "https://bitbucket.org/fenics-project/ufl/downloads/ufl-${proj_VERSION}.tar.gz")
    endif()
    set(proj_URL_MD5_0.5.4 "9a4a27d6777a3e1bc94ae2cbdfa166bd")
    set(proj_URL_MD5_0.9.0 "ec9f29bc961d35a413ddf8579d65ac3b")
    set(proj_URL_MD5_0.9.1 "2ec4c5f693cc2b87f7b825708533a799")
    set(proj_URL_MD5_1.0-beta2 "1d02f5c4c7f6ca97936ffeaba5296688")
    set(proj_URL_MD5_1.0-beta3 "4e867dc678ac870135358caf892c86d3")
    set(proj_URL_MD5_1.0-rc1 "dec26c8b11d8293602c387f969335c36")
    set(proj_URL_MD5_1.0.0 "5996ee68b1b75cb258213735a1297e51")
    set(proj_URL_MD5_1.0.1 "556afe824ab06bafaa506ac99788e41f")
    set(proj_URL_MD5_1.1.0 "59d512dc5fc50f0a251c06ee97ec5c5b")
    set(proj_URL_MD5_1.2.0 "885538bd2733fc46f6577d64240fd288")
    set(proj_URL_MD5_1.3.0 "c57de98161c10bbbc0339ef2d1a4749d")
    set(proj_URL_MD5_1.4.0 "4c04628f3dd8e68bbfe7af0f4ed583eb")
    set(proj_URL_MD5_1.5.0 "130d7829cf5a4bd5b52bf6d0955116fd")
    set(proj_URL_MD5_1.6.0 "c40c5f04eaa847377ab2323122284016")
    set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")
  else()
    set(proj_URL "http://fenicsproject.org/pub/software/ufl/ufl-snapshot.tar.gz")
    set(proj_URL_MD5 "")
  endif()

  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_build_args "--compiler=mingw32")
    set(proj_additional_install_args "--no-compile")
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )

  if (FEniCS_FORCE_REBUILD)
    externalproject_add_step(${proj} force_rebuild
      COMMENT "Force ${proj} re-build"
      DEPENDERS download
      ALWAYS 1
      WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
      )
  endif()
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
