#
# Readline
#

set(proj Readline)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})

if (FEniCS_BUILD_${proj})
  #set(proj_URL "http://pypi.python.org/packages/source/r/readline/readline-6.1.0.tar.gz")
  #set(proj_URL_MD5 "1456828f863e05a4a47c2d89823cdc4e")
  #set(proj_URL "http://pypi.python.org/packages/source/r/readline/readline-6.2.1.tar.gz")
  #set(proj_URL_MD5 "9604527863378512247fcaf938100797")
  set(proj_URL "http://pypi.python.org/packages/source/r/readline/readline-6.2.2.tar.gz")
  set(proj_URL_MD5 "ad9d4a5a3af37d31daf36ea917b08c77")

  set(proj_additional_configure_args)
  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_configure_args "--compiler=mingw32")
    set(proj_additional_build_args "--compiler=mingw32")
    set(proj_additional_install_args "--no-compile")
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
