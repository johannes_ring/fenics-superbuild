#
# ML
#

set(proj ML)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
string(TOUPPER ${proj} proj_UPPER)

if (FEniCS_BUILD_${proj})
  set(proj_URL "http://ftp.mcs.anl.gov/pub/petsc/externalpackages/ml-6.2.tar.gz")
  set(proj_URL_MD5 "c8e58389c1547459e3e2a37ae181e2a5")

  set(ep_additional_configure_args
    --disable-ml-epetra
    --disable-ml-aztecoo
    --disable-ml-examples
    --disable-tests
    --enable-mpi
    --with-mpi-compilers
    )

  if (MINGW)
    # We run cmake -P scripts to configure, build, and
    # install this external project.
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_configure_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_build_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake @ONLY)
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/ep_install_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake @ONLY)
  
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
      BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake
      INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake
      )
  else()
    externalproject_add(${proj}
      DEPENDS ${proj_DEPENDENCIES}
      URL ${proj_URL}
      URL_MD5 ${proj_URL_MD5}
      DOWNLOAD_DIR ${ep_download_dir}
      CONFIGURE_COMMAND ${ep_source_dir}/${proj}/configure --prefix=${ep_install_dir}
        ${ep_additional_configure_args}
      )
  endif()

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
