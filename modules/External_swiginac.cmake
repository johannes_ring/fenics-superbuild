#
# swiginac
#

set(proj swiginac)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})

if (FEniCS_BUILD_${proj})
  set(proj_URL "http://download.berlios.de/swiginac/swiginac-1.5.1.1.tgz")
  set(proj_URL_MD5 "f1ca51b766eeb7a46967507fbf9cb31d")

  set(proj_additional_configure_args)
  set(proj_additional_build_args)
  set(proj_additional_install_args)

  if (MINGW)
    set(proj_additional_install_args ${proj_additional_install_args} --no-compile)
  endif()

  set(proj_PATCH_COMMAND)
  if (WIN32)
    set(proj_PATCH_COMMAND
      ${PATCH_EXECUTABLE} --forward --batch --binary < ${CMAKE_CURRENT_SOURCE_DIR}/patches/swiginac_windows.patch)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    PATCH_COMMAND ${proj_PATCH_COMMAND}
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ${PYTHON_EXECUTABLE} setup.py build ${proj_additional_build_args}
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ${PYTHON_EXECUTABLE} setup.py install ${proj_additional_install_args} --prefix=${ep_python_install_dir}
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
