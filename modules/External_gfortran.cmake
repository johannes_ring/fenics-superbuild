#
# gfortran
#

set(proj gfortran)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  set(proj_URL "ftp://ftp.gnu.org/gnu/gcc/gcc-${proj_VERSION}/gcc-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_4.6.3 "c51e55130ab9ca3e5f34014cac15dd39")
  set(proj_URL_MD5_4.7.2 "5199d34506d4fa6528778ddba362d3f4")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  set(configure_args
    --enable-shared
    --enable-languages=fortran
    )

  if (DEFINED GMP_DIR)
    list(APPEND configure_args --with-gmp=${GMP_DIR})
  endif()

  if (DEFINED MPFR_DIR)
    list(APPEND configure_args --with-mpfr=${MPFR_DIR})
  endif()

  if (DEFINED MPC_DIR)
    list(APPEND configure_args --with-mpc=${MPC_DIR})
  endif()

  # Install everything in $prefix/gfortran
  list(APPEND configure_args --prefix=${ep_install_dir}/gfortran)

  ## This might be needed to find crti.o
  #if (EXISTS /usr/lib/x86_64-linux-gnu)
  #  list(APPEND configure_args CFLAGS=-B/usr/lib/x86_64-linux-gnu)
  #endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${ep_source_dir}/${proj}/configure ${configure_args}
    )

  if (NOT WIN32)
    # Create symlink $prefix/bin/gfortran -> $prefix/gfortran/bin/gfortran 
    externalproject_add_step(${proj} create_symlink
      COMMAND ${CMAKE_COMMAND} -E create_symlink ../gfortran/bin/gfortran gfortran
      COMMENT "Creating symlink for '${proj}'"
      DEPENDEES install
      #DEPENDERS install
      WORKING_DIRECTORY ${ep_install_dir}/bin
      )
  endif()

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
