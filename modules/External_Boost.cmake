#
# Boost
#

set(proj Boost)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  # Need the version to be an underscore separated string
  string(REPLACE "." "_" proj_VERSION_tmp "${proj_VERSION}")
  set(proj_URL "http://downloads.sourceforge.net/boost/boost_${proj_VERSION_tmp}.tar.gz")
  set(proj_URL_MD5_1.43.0 "734565ca4819bf04bd8e903e116c3fb1")
  set(proj_URL_MD5_1.49.0 "e0defc8c818e4f1c5bbb29d0292b76ca")
  set(proj_URL_MD5_1.51.0 "6a1f32d902203ac70fbec78af95b3cf8")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  if (MINGW AND ZLIB_DIR)
    set(bjam_additional_args "-sNO_ZLIB=0 -sZLIB_INCLUDE=${ZLIB_INCLUDE_DIRS} -sZLIB_LIBPATH=${ZLIB_LIBRARY_DIR} -sZLIB_BINARY=z")
  endif()

  # We run cmake -P scripts to configure and build this external project.
  configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_configure_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
  configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_build_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake @ONLY)

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
    BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake
    BUILD_IN_SOURCE ON
    INSTALL_COMMAND ""
    )

  if (MINGW)
    set(bjam_URL "http://downloads.sourceforge.net/boost/boost-jam/3.1.17/boost-jam-3.1.17-1-ntx86.zip")
    #set(bjam_URL_MD5 "734565ca4819bf04bd8e903e116c3fb1")
    configure_file(
      ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_download_bjam_step.cmake.in
      ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_bjam_step.cmake @ONLY)

    externalproject_add_step(${proj} download_bjam
      COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_download_bjam_step.cmake
      COMMENT "Downloading bjam for '${proj}'"
      DEPENDEES download
      DEPENDERS build
      WORKING_DIRECTORY ${ep_source_dir}/${proj}
      )
  endif()

  set(${proj_UPPER}_DIR ${ep_install_dir})
  set(Boost_CMAKE_ARGS
    -DBOOST_ROOT:PATH=${BOOST_DIR}
    -DBOOST_INCLUDEDIR:PATH=${BOOST_DIR}/include
    -DBOOST_LIBRARYDIR:PATH=${BOOST_DIR}/lib
    -DBoost_USE_MULTITHREADED:BOOL=OFF
    -DBoost_NO_SYSTEM_PATHS:BOOL=ON
    )
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
