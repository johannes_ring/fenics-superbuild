# - this module looks for MSYS
#

include(FindMinGW)

if (WIN32)
  find_path(MSYS_INSTALL_PATH
    msys.bat
    HINTS ${MSYS_DIR} $ENV{MSYS_DIR}
    PATHS
      "${MINGW_INSTALL_PATH}/msys/1.0"
      "C:/msys/1.0"
      "C:/MinGW/msys/1.0"
    )

  mark_as_advanced(MSYS_INSTALL_PATH)
endif()
