#
# PETSc
#

set(proj PETSc)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  set(proj_URL "http://ftp.mcs.anl.gov/pub/petsc/release-snapshots/petsc-lite-${proj_VERSION}.tar.gz")
  set(proj_URL_MD5_3.1-p7 "c799040c5c731c74f7f9a80e0ba27fdb")
  set(proj_URL_MD5_3.2-p7 "9272439b31313824d3c4e8f23e3e06e3")
  set(proj_URL_MD5_3.3-p3 "d0864a686e26add62f24458bd538dcca")
  set(proj_URL_MD5_3.4.5 "9b454253e587aa95096d8a9df615ab09")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  if ("${proj_VERSION}" VERSION_GREATER "3.4")
    set(PATCH_COMMAND ${PATCH_EXECUTABLE} -p1 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/petsc-3.4.patch)
  elseif ("${proj_VERSION}" VERSION_GREATER "3.2")
    set(PATCH_COMMAND ${PATCH_EXECUTABLE} -p1 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/petsc-3.3.patch)
  elseif ("${proj_VERSION}" VERSION_GREATER "3.1")
    set(PATCH_COMMAND ${PATCH_EXECUTABLE} -p1 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/petsc-3.2.patch)
  else()
    if (APPLE)
      set(PATCH_COMMAND ${PATCH_EXECUTABLE} -p1 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/petsc-mac.patch)
    else()
      set(PATCH_COMMAND ${PATCH_EXECUTABLE} -p1 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/petsc.patch)
    endif()
  endif()

  # Set PETSC_ARCH if not already defined
  if (NOT DEFINED PETSC_ARCH)
    if (APPLE)
      set(PETSC_ARCH "darwin${DARWIN_MAJOR_VERSION}.${DARWIN_MINOR_VERSION}.0-cxx-opt")
    else()
      set(PETSC_ARCH "linux-gnu-cxx-opt")
    endif()
  endif()

  if (DEFINED ATLAS_DIR)
    set(PETSc_LAPACK_args "--with-blas-lapack-dir=${ATLAS_DIR}")
  elseif (DEFINED LAPACK_DIR)
    set(PETSc_LAPACK_args "--with-blas-lapack-dir=${LAPACK_DIR}")
  endif()

  set(PETSc_UFMPACK_args)
  if (DEFINED SUITESPARSE_DIR)
    set(PETSc_UMFPACK_args "--with-umfpack=1 --with-umfpack-include=${SUITESPARSE_DIR}/include/suitesparse --with-umfpack-lib=[${SUITESPARSE_DIR}/lib/libumfpack.a,${SUITESPARSE_DIR}/lib/libamd.a]")
  endif()

  set(PETSc_ParMETIS_args)
  if (DEFINED PARMETIS_DIR)
    set(PETSc_ParMETIS_args "--with-parmetis=1 --with-parmetis-dir=${PARMETIS_DIR}")
  endif()

  set(PETSc_METIS_args)
  if (DEFINED METIS_DIR)
    set(PETSc_METIS_args "--with-metis=1 --with-metis-dir=${METIS_DIR}")
  endif()

  set(PETSc_SPOOLES_args)
  if (DEFINED SPOOLES_DIR)
    set(PETSc_SPOOLES_args "--with-spooles=1 --with-spooles-include=${SPOOLES_DIR}/include --with-spooles-lib=${SPOOLES_DIR}/lib/libspooles.a")
  endif()

  set(PETSc_Hypre_args)
  if (DEFINED HYPRE_DIR)
    set(PETSc_Hypre_args "--with-hypre=1 --with-hypre-dir=${HYPRE_DIR}")
  endif()

  set(PETSc_SCOTCH_args)
  if (DEFINED SCOTCH_DIR)
    set(PETSc_SCOTCH_args "--with-ptscotch=1 --with-ptscotch-dir=${SCOTCH_DIR}")
  endif()

  set(PETSc_ml_args)
  if (DEFINED TRILINOS_DIR)
    set(PETSc_ml_args "--with-ml=1 --with-ml-include=${TRILINOS_DIR}/include/trilinos --with-ml-lib=${TRILINOS_DIR}/lib/libml${CMAKE_SHARED_LIBRARY_SUFFIX}")
  elseif(DEFINED ML_DIR)
    set(PETSc_ml_args "--with-ml=1 --with-ml-include=${ML_DIR}/include --with-ml-lib=${ML_DIR}/lib/libml.a")
  endif()

  set(PETSc_HDF5_args)
  if (DEFINED HDF5_DIR)
    set(PETSc_HDF5_args "--with-hdf5=1 --with-hdf5-dir=${HDF5_DIR}")
  endif()

  set(PETSc_additional_args "--with-x=0 -with-x11=0")
  if (NOT CMAKE_Fortran_COMPILER_WORKS)
    set(PETSc_additional_args "${PETSc_additional_args} --with-fortran=0")
  endif()

  ## Use 64 bit integers (long long) on 64 bit arches
  #if (CMAKE_SIZEOF_VOID_P EQUAL 8)
  #  set(PETSc_additional_args "${PETSc_additional_args} --with-64-bit-indices=1")
  #endif()

  if ("${proj_VERSION}" VERSION_LESS "3.2")
    set(PETSc_additional_args "${PETSc_additional_args} --with-shared=1")
  else()
    set(PETSc_additional_args "${PETSc_additional_args} --with-shared-libraries=1")
  endif()

  # We run cmake -P scripts to configure, build, and
  # install this external project.
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_configure_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_build_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake @ONLY)
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_install_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake @ONLY)

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    PATCH_COMMAND ${PATCH_COMMAND}
    DOWNLOAD_DIR ${ep_download_dir}
    CONFIGURE_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_configure_step.cmake
    BUILD_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_build_step.cmake
    INSTALL_COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_step.cmake
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
  fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
