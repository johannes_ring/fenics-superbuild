#
# VTK
#

set(proj VTK)
set(proj_DEPENDENCIES ${${proj}_DEPENDENCIES})
set(proj_VERSION ${${proj}_VERSION})
string(TOUPPER ${proj} proj_UPPER)

# Sanity checks
if(DEFINED ${proj_UPPER}_DIR AND NOT EXISTS ${${proj_UPPER}_DIR})
  message(FATAL_ERROR "${proj_UPPER}_DIR variable is defined but corresponds to non-existing directory.")
endif()

if (FEniCS_BUILD_${proj} AND NOT DEFINED ${proj_UPPER}_DIR)
  string(REGEX REPLACE "^([0-9]+).*$" "\\1" VTK_MAJOR_VERSION "${proj_VERSION}")
  string(REGEX REPLACE "^[0-9]+\\.([0-9]+).*$" "\\1" VTK_MINOR_VERSION "${proj_VERSION}")
  string(REGEX REPLACE "^[0-9]+\\.[0-9]+\\.([0-9]+).*$" "\\1" VTK_BUILD_VERSION "${proj_VERSION}")

  set(proj_URL "http://www.vtk.org/files/release/${VTK_MAJOR_VERSION}.${VTK_MINOR_VERSION}/vtk-${VTK_VERSION}.tar.gz")
  set(proj_URL_MD5_5.6.0 "a56232baae6071f9f59f138d8f4620c2")
  set(proj_URL_MD5_5.8.0 "37b7297d02d647cc6ca95b38174cb41f")
  set(proj_URL_MD5_5.10.0 "a0363f78910f466ba8f1bd5ab5437cb9")
  set(proj_URL_MD5_5.10.1 "264b0052e65bd6571a84727113508789")
  set(proj_URL_MD5 "${proj_URL_MD5_${proj_VERSION}}")

  set(proj_PATCH_COMMAND "")
  if ("${VTK_MAJOR_VERSION}.${VTK_MINOR_VERSION}" VERSION_EQUAL "5.6")
    if (MINGW)
      set(proj_PATCH_COMMAND
	${PATCH_EXECUTABLE} -p1 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/vtk5.6_no_directx.patch)
    elseif (APPLE)
      set(proj_PATCH_COMMAND
	${PATCH_EXECUTABLE} -p1 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/vtk5.6-osx.patch)
    endif()
  endif()

  if ("${VTK_MAJOR_VERSION}.${VTK_MINOR_VERSION}" VERSION_EQUAL "5.10")
    set(proj_PATCH_COMMAND
      ${PATCH_EXECUTABLE} -p1 --forward --batch < ${FEniCS_SOURCE_DIR}/patches/vtk_tiff_inline.patch)
  endif()

  set(proj_additional_args)
  if (WIN32 OR APPLE)
    set(proj_additional_args
      -D VTK_USE_TK:BOOL=OFF)
    if (MINGW)
      set(proj_additional_args
        ${proj_additional_args}
        -D CMAKE_USE_PTHREADS:BOOL=OFF
        -D VTK_USE_GCC_VISIBILITY:BOOL=OFF
	-D VTK_USE_DIRECTX:BOOL=OFF)
    endif()
  else()
    set(proj_additional_args
      -DVTK_USE_TK:BOOL=ON)
  endif()

  set (VTK_Python_ARGS)
  if (WIN32)
    set(VTK_Python_args
      ${VTK_Python_args}
      -D VTK_INSTALL_NO_PYTHON:BOOL=ON)
  else()
    set(VTK_Python_args
      ${VTK_Python_args}
      -D VTK_INSTALL_PYTHON_USING_CMAKE:BOOL=ON)
  endif()

  externalproject_add(${proj}
    DEPENDS ${proj_DEPENDENCIES}
    URL ${proj_URL}
    URL_MD5 ${proj_URL_MD5}
    PATCH_COMMAND ${proj_PATCH_COMMAND}
    DOWNLOAD_DIR ${ep_download_dir}
    CMAKE_GENERATOR ${CMAKE_GENERATOR}
    CMAKE_ARGS
      ${ep_common_args}
      #-D CMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS}
      -D CMAKE_VERBOSE_MAKEFILE:BOOL=OFF
      -D VTK_WRAP_PYTHON:BOOL=ON
      -D VTK_WRAP_TCL:BOOL=OFF
      -D VTK_USE_RPATH:BOOL=OFF
      -D VTK_USE_GL2PS:BOOL=ON
      ${proj_additional_args}
      ${VTK_Python_args}
    )

  # We add an additional step to install the VTK Python wrappers
  configure_file(
    ${CMAKE_CURRENT_SOURCE_DIR}/templates/${proj}_install_python_step.cmake.in
    ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_python_step.cmake @ONLY)

  externalproject_add_step(${proj} install_python
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/${proj}_install_python_step.cmake
    COMMENT "Installing Python wrappers for '${proj}'"
    DEPENDEES install
    WORKING_DIRECTORY ${ep_source_dir}/${proj}-build
    )

  set(${proj_UPPER}_DIR ${ep_install_dir})
else()
    fenicsmacro_empty_externalproject(${proj} "${proj_DEPENDENCIES}")
endif()
